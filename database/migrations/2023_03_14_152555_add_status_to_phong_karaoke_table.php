<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('phong_karaoke', function (Blueprint $table) {
            $table->char('tinh_trang',1)->default(0)->comment('0/Phong trong 1/Dang Thue');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('phong_karaoke', function (Blueprint $table) {
            //
        });
    }
};
