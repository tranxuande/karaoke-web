<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('phong_karaoke', function (Blueprint $table) {
            $table->id();
            $table->text('image');
            $table->string('ten_phong');
            $table->double('gia_1_gio');
            $table->bigInteger('id_loai_phong')->unsigned();
            $table->foreign('id_loai_phong')->references('id')->on('loai_phong_karaoke');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('phong_karaoke');
    }
};
