<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('san_pham', function (Blueprint $table) {
            $table->id();
            $table->string('ten_san_pham');
            $table->string('loai_san_pham');
            $table->integer('so luong nhap');
            $table->string('don_vi_nhap');
            $table->double('gia_nhap');
            $table->double('gia_ban');
            $table->string('don_vi_tinh');
            $table->bigInteger('id_danh_muc_sp')->unsigned();
            $table->foreign('id_danh_muc_sp')->references('id')->on('danh_muc_san_pham');
            $table->char('tinh_trang',1)->default('1')->comment('0/het hang 1/con hang');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('san_pham');
    }
};
