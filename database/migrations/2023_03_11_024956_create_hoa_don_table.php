<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hoa_don', function (Blueprint $table) {
            $table->id();
            $table->dateTime('thoi_gian_vao');
            $table->dateTime('thoi_gian_ra');
            $table->dateTime('ngay_tao');
            $table->bigInteger('id_khach_hang')->unsigned();
            $table->foreign('id_khach_hang')->references('id')->on('khach_hang');
            $table->bigInteger('id_phong')->unsigned();
            $table->foreign('id_phong')->references('id')->on('phong_karaoke');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hoa_don');
    }
};
