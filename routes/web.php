<?php

use App\Http\Controllers\admin\AdminController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['controller' => UserController::class], function () {
    Route::get('', 'home')->name('home');
    Route::post('','postLogin')->name('login');
    Route::get('register','register')->name('register');
    Route::post('register','postRegister')->name('register');
    // Route::get('home','home')->name('home');
    Route::get('detail/{id}','detail')->name('detail');
    Route::post('detail/{is}','postDetail')->name('detail');
    Route::get('bill','bill')->name('bill');
    Route::post('find/{id}','find');
    Route::get('getBill/{id}','getBill');
   
    Route::post('confirm/{id}','confirm')->name('confirm');
    Route::post('confirm-hd','postConfirm')->name('confirm-hd');

});

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'controller' => AdminController::class], function () {
    Route::get('login','login')->name('login');
    Route::post('login','postlogin')->name('login');
    
    Route::get('', 'index')->name('index')->middleware('checkLogin');

    Route::post('filter-results', 'filterResult');

    // room
    Route::get('list-room', 'listRoom')->name('list-room');

    Route::get('empty-room', 'emptyRoom')->name('empty-room');

    Route::get('create-room', 'createRoom')->name('create-room');
    Route::post('create-room', 'postCreateRoom')->name('create-room');

    Route::get('edit-room/{id}', 'editRoom')->name('edit-room');
    Route::post('edit-room/{id}', 'postEditRoom')->name('edit-room');

    Route::post('delete-room/{id}','deleteRoom')->name('delete-room');
    Route::post('cancel/{id}','cancel')->name('cancel');

    Route::post('find/{id}','findHD');

    Route::post('start-room/{id}','startRoom');

    // product
    Route::get('list-product','listProduct')->name('list-product');
    Route::post('add-product','addProduct')->name('add-product');
    Route::post('edit-product','editProduct')->name('edit-product');

    Route::get('list-import-product', 'listImportProduct')->name('list-import-product');
    Route::get('import-product', 'importProduct')->name('import-product');
    Route::post('import-product', 'postImportProduct')->name('import-product');

    Route::get('list-export-product', 'listExportProduct')->name('list-export-product');
    Route::get('export-product/{id}', 'exportProduct')->name('export-product');
    Route::post('export-product/{id}', 'postExportProduct')->name('export-product');

    Route::get('hoadon/{id}','detailHd')->name('detailBill');

    Route::get('khach-hang','customer')->name('customer');

}); 



