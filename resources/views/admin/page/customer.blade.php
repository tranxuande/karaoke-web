@extends('admin.layout.main')
@section('style')
<style>
    .content {
        width: 80%;
        margin: 50px auto;
    }
</style>
@endsection
@section('content')
<div class="conatiner-fluid content">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    <div class="header-title">
                        <h4 class="card-title">Thông tin khách hàng</h4>
                    </div>
                </div>
                <div class="card-body">
                <div class="table-responsive mt-4">
                        <table id="basic-table" class="table table-striped mb-0" role="grid">
                            <thead>
                                <tr>
                                    <th>Tên khách hàng</th>
                                    <th>Số điện thoại</th>
                                    <th>Hóa đơn</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($listUser as $i)
                                <tr>
                                    <td>{{$i->ten_khach_hang}}</td>
                                    <td>{{$i->sdt}}</td>
                                    <td>{{$i->tong}}</td>                                   
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection