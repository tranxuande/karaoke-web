@extends('admin.layout.main')
@section('style')
<style>
    #chart {
        max-width: 100%;
    }
</style>

@endsection
@section('nav')
<div class="iq-navbar-header" style="height: 215px;">
    <div class="container-fluid iq-container">
        <div class="row">
            <div class="col-md-12">
                <div class="flex-wrap d-flex justify-content-between align-items-center">
                    <div>
                        <h1>Hello Devs!</h1>
                        <p>We are on a mission to help developers like you build successful projects for
                            FREE.</p>
                    </div>
                    <div>
                        <a href="" class="btn btn-link btn-soft-light">
                            <svg class="icon-20" width="20" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M11.8251 15.2171H12.1748C14.0987 15.2171 15.731 13.985 16.3054 12.2764C16.3887 12.0276 16.1979 11.7713 15.9334 11.7713H14.8562C14.5133 11.7713 14.2362 11.4977 14.2362 11.16C14.2362 10.8213 14.5133 10.5467 14.8562 10.5467H15.9005C16.2463 10.5467 16.5263 10.2703 16.5263 9.92875C16.5263 9.58722 16.2463 9.31075 15.9005 9.31075H14.8562C14.5133 9.31075 14.2362 9.03619 14.2362 8.69849C14.2362 8.35984 14.5133 8.08528 14.8562 8.08528H15.9005C16.2463 8.08528 16.5263 7.8088 16.5263 7.46728C16.5263 7.12575 16.2463 6.84928 15.9005 6.84928H14.8562C14.5133 6.84928 14.2362 6.57472 14.2362 6.23606C14.2362 5.89837 14.5133 5.62381 14.8562 5.62381H15.9886C16.2483 5.62381 16.4343 5.3789 16.3645 5.13113C15.8501 3.32401 14.1694 2 12.1748 2H11.8251C9.42172 2 7.47363 3.92287 7.47363 6.29729V10.9198C7.47363 13.2933 9.42172 15.2171 11.8251 15.2171Z" fill="currentColor"></path>
                                <path opacity="0.4" d="M19.5313 9.82568C18.9966 9.82568 18.5626 10.2533 18.5626 10.7823C18.5626 14.3554 15.6186 17.2627 12.0005 17.2627C8.38136 17.2627 5.43743 14.3554 5.43743 10.7823C5.43743 10.2533 5.00345 9.82568 4.46872 9.82568C3.93398 9.82568 3.5 10.2533 3.5 10.7823C3.5 15.0873 6.79945 18.6413 11.0318 19.1186V21.0434C11.0318 21.5715 11.4648 22.0001 12.0005 22.0001C12.5352 22.0001 12.9692 21.5715 12.9692 21.0434V19.1186C17.2006 18.6413 20.5 15.0873 20.5 10.7823C20.5 10.2533 20.066 9.82568 19.5313 9.82568Z" fill="currentColor"></path>
                            </svg>
                            Announcements
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="iq-header-img">
        <img src="../assets/images/dashboard/top-header.png" alt="header" class="theme-color-default-img img-fluid w-100 h-100 animated-scaleX">
        <img src="../assets/images/dashboard/top-header1.png" alt="header" class="theme-color-purple-img img-fluid w-100 h-100 animated-scaleX">
        <img src="../assets/images/dashboard/top-header2.png" alt="header" class="theme-color-blue-img img-fluid w-100 h-100 animated-scaleX">
        <img src="../assets/images/dashboard/top-header3.png" alt="header" class="theme-color-green-img img-fluid w-100 h-100 animated-scaleX">
        <img src="../assets/images/dashboard/top-header4.png" alt="header" class="theme-color-yellow-img img-fluid w-100 h-100 animated-scaleX">
        <img src="../assets/images/dashboard/top-header5.png" alt="header" class="theme-color-pink-img img-fluid w-100 h-100 animated-scaleX">
    </div>
</div>
@endsection
@section('content')

<div class="conatiner-fluid content-inner my-5 py-0">
    <form class="form-horizontal py-5">
        <div class="row">
            <div class="form-group col-4 ">
                <div class="form-group">
                    <label class="form-label" for="exampleFormControlSelect1">Chọn tháng</label>
                    <select class="form-select" id="month">
                        <option selected="" disabled="">Select month</option>
                        @for($i=1;$i<=12;$i++) <option value='{{$i}}'>Tháng {{$i}}</option>
                            @endfor
                    </select>
                </div>
            </div>
            <div class="form-group col-4 ">
                <div class="form-group">
                    <label class="form-label" for="exampleFormControlSelect1">Chọn năm</label>
                    <select class="form-select" id="year">
                        <option selected="" disabled="">Select year</option>
                        @php
                        $year = date('Y', strtotime(\Carbon\Carbon::now()));
                        @endphp
                        @for($i=2020;$i<=$year;$i++) <option value='{{$i}}'>Năm {{$i}}</option>
                            @endfor
                    </select>
                </div>
            </div>
            <div class="form-group col-3 text-center" style="margin-top:32px;">
                <button type="button" class="btn btn-primary btn-search">Lọc kết quả</button>
            </div>
        </div>
    </form>
    <div class="row">
        <h4 class="card-title mb-3">Tổng quan kinh doanh tháng <span id="overview-month">{{$month}}</span></h4>
        <div class="col-xl-4">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex align-items-center justify-content-between">
                        <div class=" bg-soft-success rounded p-3">
                            <svg class="icon-35" xmlns="http://www.w3.org/2000/svg" width="35px" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 8h6m-5 0a3 3 0 110 6H9l3 3m-3-6h6m6 1a9 9 0 11-18 0 9 9 0 0118 0z"></path>
                            </svg>
                        </div>
                        <div>
                            <h3 class="text-success counter text-center" id='total-revenue'>{{$totalRevenue}}</h3>
                            <p class="text-success mb-0">Tổng doanh thu</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-4">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex align-items-center justify-content-between">
                        <div class=" bg-soft-success rounded p-3">
                            <i class="fas fa-users"></i>
                        </div>
                        <div>
                            <h3 class="text-success counter text-center" id="total-customer">{{$totalCustomer}}</h3>
                            <p class="text-success mb-0">Tổng số khách hàng</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-4">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex align-items-center justify-content-between">
                        <div class=" bg-soft-success rounded p-3">
                            <i class=" fas fa-duotone fa-ballot-check"></i>
                        </div>
                        <div>
                            <h3 class="text-success counter text-center" id="total-bill">{{$countBill}}</h3>
                            <p class="text-success mb-0">Tổng số hóa đơn</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="chart1"></div>
    <div class="row">
        <div class="col-4">
            <div class="card">
                <h5 style="padding: 15px 0 15px 15px;">Tỉ lệ đặt phòng</h5>
                <div id="chart2"></div>
            </div>
        </div>
        <div class="col-8">
            <div class="card">

            </div>
        </div>
    </div>
</div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    $('.home').addClass('active');

    function chartView(dates, totalsByDate) {

        var options = {
            chart: {
                type: 'line',
                height: 400,
                borderRadius: 10,
                background: '#FFFFFF',
            },
            series: [{
                name: 'total',
                data: totalsByDate,
            }],
            xaxis: {
                categories: dates
            },
            tooltip: {
                y: {
                    formatter: function(value, {
                        seriesIndex,
                        w
                    }) {
                        return Number(value).toLocaleString(undefined, {
                            minimumFractionDigits: 2, // set the minimum number of decimal places to 2
                            maximumFractionDigits: 2 // set the maximum number of decimal places to 2
                        });
                    }
                }
            }
        }

        var chart1 = new ApexCharts(document.querySelector("#chart1"), options);

        chart1.render();
    }

    var dates = @json($dates);
    var totalsByDate = @json($totalsByDate);
    chartView(dates, totalsByDate);

    let numberRoom = @json($numberRoom);
    // let seri = [],
    //     label = [];
    // numberRoom.forEach(function(ele) {
    //     seri.push(ele.tong);
    //     label.push(ele.ten_loai_phong);
    // })
    chartView2(numberRoom);

    function chartView2(numberRoom) {
        let seri = [],
            label = [];
        numberRoom.forEach(function(ele) {
            seri.push(ele.tong);
            label.push(ele.ten_loai_phong);
        })
        let options = {
            chart: {
                width: 380,
                type: "donut"
            },
            colors: ["#006600", "#0084ff", "#CCCC00", "#990033"],
            dataLabels: {
                enabled: true
            },
            series: seri,
            labels: label,

            tooltip: {
                enabled: true,
                x: {
                    title: {
                        formatter: function(seriesName) {
                            return seriesName
                        }
                    }
                }
            },
            legend: {
                show: true
            }
        };

        let chart2 = new ApexCharts(document.querySelector("#chart2"), options);

        chart2.render();
    }
    // var options = {
    //     chart: {
    //         width: 380,
    //         type: "donut"
    //     },
    //     colors: ["#006600", "#0084ff", "#CCCC00", "#990033"],
    //     dataLabels: {
    //         enabled: true
    //     },
    //     series: seri,
    //     labels: label,

    //     tooltip: {
    //         enabled: true,
    //         x: {
    //             title: {
    //                 formatter: function(seriesName) {
    //                     return seriesName
    //                 }
    //             }
    //         }
    //     },
    //     legend: {
    //         show: true
    //     }
    // };

    // var chart2 = new ApexCharts(document.querySelector("#chart2"), options);

    // chart2.render();




    $('.btn-search').click(function() {
        let month = $('#month').val(),
            year = $('#year').val();
        if (month != null && year != null) {
            $.ajax({
                type: 'post',
                url: '/admin/filter-results',
                data: {
                    month: month,
                    year: year
                },
                success: function(result) {
                    $("#overview-month").text(month);
                    $('#total-revenue').text(result.totalRevenue);
                    $('#total-customer').text(result.totalCustomer);
                    $('#total-bill').text(result.totalBill);

                    if (result.totalRevenue != 0) {
                        chartView(result.dates, result.totalsByDate);

                        chartView2(result.numberRoom);
                    } else {
                        $('#chart1').text('Not have data')
                        $('#chart2').text('Not have data')
                    }

                },
                error: function() {
                    console.log('err')
                }
            });
        }
    })
</script>

@endsection