@extends('admin.layout.main')
@section('style')
<style>
    .content {
        width: 90%;
        margin: 50px auto;
    }
</style>
@endsection
@section('content')
<div class="conatiner-fluid content">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    <div class="header-title">
                        <h4 class="card-title">Custom Validation</h4>
                    </div>
                </div>
                <div class="card-body">
                    <form class="row g-3 needs-validation" id="myform" novalidate action="{{ route('admin.import-product') }}" method="post">
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class="mb-5">
                                    <label for="validationCustom02" class="form-label">Ten</label>
                                    <select class="form-select" id="tenSp" required name="id_sp">
                                        <option selected disabled value="">Choose san pham</option>
                                        @foreach($list as $i)
                                        <option value="{{$i->id}}">{{$i->ten_san_pham}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <!--<div class="col-md-6">
                                <div class="mb-5">
                                    <label for="validationCustom02" class="form-label">Loai san pham</label>
                                    <select class="form-select" id="loaiSp" readonly name="loai_san_pham">
                                        <option selected disabled value="">Choose...</option>
                                        <option value="0">Do an</option>
                                        <option value="1">Do uong</option>
                                    </select>
                                </div>
                            </div>-->
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="mb-5">
                                    <label for="validationCustom02" class="form-label">So luong nhap</label>
                                    <input type="text" class="form-control" id="sln" name="so_luong_nhap" required>
                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="mb-5">
                                    <label for="validationCustom02" class="form-label">don vi nhap</label>
                                    <select class="form-select"  required name="don_vi_nhap">
                                        <option selected disabled value="">Choose...</option>
                                        <option>Thung</option>
                                        <option>Lit</option>
                                        <option>Kg</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="mb-5">
                                    <label for="validationCustom02" class="form-label">So luong/sp</label>
                                    <input type="text" class="form-control" id="sl1sp" name="so_luong_1_sp">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="mb-5">
                                    <label for="validationCustom02" class="form-label">Gia nhap</label>
                                    <input type="text" class="form-control" id="gianhap" name="gia_nhap" required>
                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <button class="btn btn-primary" type="submit">Submit form</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('script')
<script>
    const sln = document.getElementById('sln');
    const sl1sp = document.getElementById('sl1sp');
    const gianhap = document.getElementById('gianhap');

    sln.addEventListener('keypress', function(event) {
        const keyCode = event.keyCode || event.which;
        const keyValue = String.fromCharCode(keyCode);

        // Chỉ cho phép nhập số và dấu chấm
        const regex = /^[0-9.]+$/;
        if (!regex.test(keyValue)) {
            event.preventDefault();
        }
    });
    sl1sp.addEventListener('keypress', function(event) {
        const keyCode = event.keyCode || event.which;
        const keyValue = String.fromCharCode(keyCode);

        // Chỉ cho phép nhập số và dấu chấm
        const regex = /^[0-9.]+$/;
        if (!regex.test(keyValue)) {
            event.preventDefault();
        }
    });
    gianhap.addEventListener('keypress', function(event) {
        const keyCode = event.keyCode || event.which;
        const keyValue = String.fromCharCode(keyCode);

        // Chỉ cho phép nhập số và dấu chấm
        const regex = /^[0-9.]+$/;
        if (!regex.test(keyValue)) {
            event.preventDefault();
        }
    });

    sln.addEventListener('keyup', function(event) {
        const value = event.target.value;

        // Xóa tất cả ký tự không phải số hoặc dấu chấm
        const formattedValue = value.replace(/[^0-9.]/g, '');

        // Tạo định dạng có dấu phẩy
        const parts = formattedValue.split('.');
        const integerPart = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',');
        const decimalPart = parts.length > 1 ? '.' + parts[1] : '';
        const formattedNumber = integerPart + decimalPart;

        // Cập nhật giá trị vào ô input
        event.target.value = formattedNumber;
    });

    gianhap.addEventListener('keyup', function(event) {
        const value = event.target.value;

        // Xóa tất cả ký tự không phải số hoặc dấu chấm
        const formattedValue = value.replace(/[^0-9.]/g, '');

        // Tạo định dạng có dấu phẩy
        const parts = formattedValue.split('.');
        const integerPart = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',');
        const decimalPart = parts.length > 1 ? '.' + parts[1] : '';
        const formattedNumber = integerPart + decimalPart;

        // Cập nhật giá trị vào ô input
        event.target.value = formattedNumber;
    });

    $('#loaiSp').change(function(){
        
        if($(this).val()==0){
            $('input[name="so_luong_1_sp"]').prop('disabled', true);
        }
        else{
            $('input[name="so_luong_1_sp"]').prop('disabled', false);
        }
        
    })

    $('#myform').on('submit', function() {
        var sln = $('#sln').val();
        var cleanedVal1 = sln.replace(/,/g, '');
        $('#sln').val(cleanedVal1);

        var gianhap = $('#gianhap').val();
        var cleanedVal2 = gianhap.replace(/,/g, '');
        $('#gianhap').val(cleanedVal2);
    });

    $('.import-product').addClass('active');
</script>
@endsection