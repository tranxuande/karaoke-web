@extends('admin.layout.main')
@section('style')
<style>
    .icon-32 {
        cursor: pointer;
    }

    #paginate {
        margin: 20px auto;
    }
</style>
@endsection
@section('content')

<div class="conatiner-fluid  ">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">

                @if (session('mess'))
                <div class="alert alert-left alert-success alert-dismissible fade show mb-3" role="alert">
                    <span> This is a success alert—check it out!</span>
                    <button type="button" class="btn-close btn-close-white" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
                @endif

                <div class="card-header d-flex justify-content-between">
                    <div class="header-title">
                        <h4 class="card-title">Danh sách sản phẩm</h4>
                    </div>
                    <a href="{{route('admin.import-product')}}">
                        <button type="button" class="btn btn-primary"> <svg class="icon-32" width="32" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M12.0001 8.32739V15.6537" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                <path d="M15.6668 11.9904H8.3335" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M16.6857 2H7.31429C4.04762 2 2 4.31208 2 7.58516V16.4148C2 19.6879 4.0381 22 7.31429 22H16.6857C19.9619 22 22 19.6879 22 16.4148V7.58516C22 4.31208 19.9619 2 16.6857 2Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                            </svg> Nhập sản phẩm</button>
                    </a>
                </div>
                <div class="card-body p-0">
                    <div class="table-responsive mt-4">
                        <table id="basic-table" class="table table-striped mb-0" role="grid">
                            <thead>
                                <tr>
                                    <th>STT</th>
                                    <th>Tên</th>
                                    <th>Số lượng nhập</th>
                                    <th>Đơn vị nhập</th>
                                    <th>Giá nhập/don vi</th>
                                    <th>Thời gian nhập</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php $count=1 @endphp
                                @foreach($listProduct as $i)
                                <tr>
                                    <td>{{$count++}}</td>
                                    <td>
                                        {{$i->sanPham->ten_san_pham}}
                                    </td>
                                    <td class="format-price">{{$i->so_luong_nhap}}</td>
                                    <td>
                                        {{$i->don_vi_nhap}}
                                    </td>
                                    <td class="format-price">
                                        {{$i->gia_nhap}}
                                    </td>
                                    <td>
                                        {{$i->thoi_gian_nhap}}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div id="paginate">

                </div>

            </div>
        </div>
    </div>
</div>

@endsection
@section('script')

<script>
    $('.import-product').addClass('active');

    function format(number) {
        return number.toLocaleString('en-US');
    }

    $('.format-price').each(function() {
        let format = $.number($(this).text(), {
            style: "decimal"
        });

        $(this).text(format);
    })
</script>
@endsection