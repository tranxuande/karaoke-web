@extends('admin.layout.main')
@section('style')
<style>
    .content {
        width: 90%;
        margin: 50px auto;
    }
</style>
@endsection
@section('content')
<div class="conatiner-fluid content">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    <div class="header-title">
                        <h4 class="card-title">Custom Validation</h4>
                    </div>
                </div>
                <div class="card-body">
                    <form class="row g-3 needs-validation" id="myform" novalidate action="{{ route('admin.export-product', $product->id) }}" method="post">
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class="mb-5">
                                    <label for="validationCustom02" class="form-label">Ten</label>
                                    <input type="text" class="form-control" id="validationCustom02" value="{{$product->ten_san_pham}}" readonly>
                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="mb-5">
                                    <label for="validationCustom02" class="form-label">Gia ban</label>
                                    <input type="text" class="form-control" id="sln" name="gia_ban" required>
                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="mb-5">
                                    <label for="validationCustom02" class="form-label">don vi ban</label>
                                    <select class="form-select" id="validationCustom04" required name="don_vi_ban">
                                        <option selected disabled value="">Choose...</option>
                                        <option>Chai</option>
                                        <option>Lon</option>
                                        <option>Kg</option>
                                        <option>phan</option>
                                    </select>
                                </div>
                            </div>
                            @if($product->loai_san_pham==0)
                            <div class="col-md-1" style="font-size: 60px;">~</div>
                            <div class="col-md-3">

                                <div class="mb-5">
                                    <label for="validationCustom02" class="form-label">Dinh luong</label>
                                    <input type="text" class="form-control" id="sl1sp" name="dinh_luong" required>
                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="mb-5">
                                    <label for="validationCustom02" class="form-label">don vi </label>
                                    <select class="form-select" id="validationCustom04" required name="don_vi_dinh_luong">
                                        <option selected disabled value="">Choose...</option>
                                        <option>g</option>
                                        <option>kg</option>
                                        <option>yen</option>
                                    </select>
                                </div>
                            </div>
                            @endif
                        </div>
                        <div class="col-12">
                            <button class="btn btn-primary" type="submit">Submit form</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('script')
<script>
    $('.export-product').addClass('active');
    
    const sln = document.getElementById('sln');
    const sl1sp = document.getElementById('sl1sp');
    const gianhap = document.getElementById('gianhap');

    sln.addEventListener('keypress', function(event) {
        const keyCode = event.keyCode || event.which;
        const keyValue = String.fromCharCode(keyCode);

        // Chỉ cho phép nhập số và dấu chấm
        const regex = /^[0-9.]+$/;
        if (!regex.test(keyValue)) {
            event.preventDefault();
        }
    });
    sl1sp.addEventListener('keypress', function(event) {
        const keyCode = event.keyCode || event.which;
        const keyValue = String.fromCharCode(keyCode);

        // Chỉ cho phép nhập số và dấu chấm
        const regex = /^[0-9.]+$/;
        if (!regex.test(keyValue)) {
            event.preventDefault();
        }
    });
    gianhap.addEventListener('keypress', function(event) {
        const keyCode = event.keyCode || event.which;
        const keyValue = String.fromCharCode(keyCode);

        // Chỉ cho phép nhập số và dấu chấm
        const regex = /^[0-9.]+$/;
        if (!regex.test(keyValue)) {
            event.preventDefault();
        }
    });

    sln.addEventListener('keyup', function(event) {
        const value = event.target.value;

        // Xóa tất cả ký tự không phải số hoặc dấu chấm
        const formattedValue = value.replace(/[^0-9.]/g, '');

        // Tạo định dạng có dấu phẩy
        const parts = formattedValue.split('.');
        const integerPart = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',');
        const decimalPart = parts.length > 1 ? '.' + parts[1] : '';
        const formattedNumber = integerPart + decimalPart;

        // Cập nhật giá trị vào ô input
        event.target.value = formattedNumber;
    });

    gianhap.addEventListener('keyup', function(event) {
        const value = event.target.value;

        // Xóa tất cả ký tự không phải số hoặc dấu chấm
        const formattedValue = value.replace(/[^0-9.]/g, '');

        // Tạo định dạng có dấu phẩy
        const parts = formattedValue.split('.');
        const integerPart = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',');
        const decimalPart = parts.length > 1 ? '.' + parts[1] : '';
        const formattedNumber = integerPart + decimalPart;

        // Cập nhật giá trị vào ô input
        event.target.value = formattedNumber;
    });


    $('#myform').on('submit', function() {
        var sln = $('#sln').val();
        var cleanedVal1 = sln.replace(/,/g, '');
        $('#sln').val(cleanedVal1);

        var gianhap = $('#gianhap').val();
        var cleanedVal2 = gianhap.replace(/,/g, '');
        $('#gianhap').val(cleanedVal2);
    });

    
  
</script>
@endsection