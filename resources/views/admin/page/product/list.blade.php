@extends('admin.layout.main')
@section('style')
<style>
    .overlay {
        position: fixed;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        background: rgba(0, 0, 0, 0.7);
        transition: opacity 500ms;
        visibility: hidden;
        opacity: 0;

    }

    .overlay:target {
        visibility: visible;
        opacity: 1;
    }

    .popup {
        margin: 70px auto;
        padding: 20px;
        background: #fff;
        border-radius: 5px;
        width: 500px;
        height: 300px;
        position: relative;
    }

    /* .content {
        height: 500px;

        overflow-x: hidden !important;
        
    } */

    .popup h2 {
        margin-top: 0;
        color: #333;
        font-family: Tahoma, Arial, sans-serif;
    }

    .popup .close {
        position: absolute;
        top: 20px;
        right: 30px;
        transition: all 200ms;
        font-size: 30px;
        font-weight: bold;
        text-decoration: none;
        color: #333;
    }

    .popup .close:hover {
        color: #06D85F;
    }

    .popup .content {
        max-height: 30%;
        overflow: auto;
    }
</style>
@endsection
@section('content')

<div class="conatiner-fluid  ">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">

                @if (session('mess'))
                <div class="alert alert-left alert-success alert-dismissible fade show mb-3" role="alert">
                    <span>{{session('mess')}}</span>
                    <button type="button" class="btn-close btn-close-white" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
                @endif

                <div class="card-header d-flex justify-content-between">
                    <div class="header-title">
                        <h4 class="card-title">Danh sách sản phẩm</h4>
                    </div>

                    <a class="button" href="#popup1">
                        <button type="button" class="btn btn-primary"> <svg class="icon-32" width="32" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M12.0001 8.32739V15.6537" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                <path d="M15.6668 11.9904H8.3335" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M16.6857 2H7.31429C4.04762 2 2 4.31208 2 7.58516V16.4148C2 19.6879 4.0381 22 7.31429 22H16.6857C19.9619 22 22 19.6879 22 16.4148V7.58516C22 4.31208 19.9619 2 16.6857 2Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                            </svg> Thêm sản phẩm</button>
                    </a>

                </div>
                <div class="card-body p-0">
                    <div class="table-responsive mt-4">
                        <table id="basic-table" class="table table-striped mb-0" role="grid">
                            <thead>
                                <tr>
                                    <th>Tên</th>
                                    <th>Loại mặt hàng</th>
                                    <th>Số lượng có</th>
                                    <th>Tình trạng</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach($list as $i)
                                
                                <tr>

                                    <td>
                                        {{$i->ten_san_pham}}
                                    </td>
                                    <td>{{$i->loai_san_pham == 0 ? 'Do an' : 'Do uong'}}</td>
                                    <td>
                                        {{$i->so_luong_co}}
                                    </td>
                                    <td>
                                        {{$i->tinh_trang == 0 ? 'Het hang' : 'Con hang'}}
                                    </td>
                                    <td> <a href="#popup2" sp='{{$i}}' class="edit">
                                            <svg class="icon-32 mr-3" width="" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd" clip-rule="evenodd" d="M16.6653 2.01034C18.1038 1.92043 19.5224 2.41991 20.5913 3.3989C21.5703 4.46779 22.0697 5.88633 21.9898 7.33483V16.6652C22.0797 18.1137 21.5703 19.5322 20.6013 20.6011C19.5323 21.5801 18.1038 22.0796 16.6653 21.9897H7.33487C5.88636 22.0796 4.46781 21.5801 3.39891 20.6011C2.41991 19.5322 1.92043 18.1137 2.01034 16.6652V7.33483C1.92043 5.88633 2.41991 4.46779 3.39891 3.3989C4.46781 2.41991 5.88636 1.92043 7.33487 2.01034H16.6653ZM10.9811 16.845L17.7042 10.102C18.3136 9.4826 18.3136 8.48364 17.7042 7.87427L16.4056 6.57561C15.7862 5.95625 14.7872 5.95625 14.1679 6.57561L13.4985 7.25491C13.3986 7.35481 13.3986 7.52463 13.4985 7.62453C13.4985 7.62453 15.0869 9.20289 15.1169 9.24285C15.2268 9.36273 15.2967 9.52256 15.2967 9.70238C15.2967 10.062 15.007 10.3617 14.6374 10.3617C14.4675 10.3617 14.3077 10.2918 14.1978 10.1819L12.5295 8.5236C12.4496 8.44368 12.3098 8.44368 12.2298 8.5236L7.46474 13.2887C7.13507 13.6183 6.94527 14.0579 6.93528 14.5274L6.87534 16.8949C6.87534 17.0248 6.9153 17.1447 7.00521 17.2346C7.09512 17.3245 7.21499 17.3744 7.34486 17.3744H9.69245C10.172 17.3744 10.6315 17.1846 10.9811 16.845Z" fill="currentColor"></path>
                                            </svg>
                                        </a></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div id="paginate">

                </div>

            </div>
        </div>
    </div>
</div>
<div id="popup1" class="overlay">
    <div class="popup">
        <h2>Thêm sản phẩm</h2>
        <a class="close" href="#">&times;</a>
        <form action="{{route('admin.add-product')}}" method="post">
            @csrf
            <div class="mt-4">
                <input class="form-control" name="ten_sp" placeholder="Ten san pham">
                <div><br /></div>
                <select class="form-select" name="loai_sp">
                    <option value="0">Đồ ăn</option>
                    <option value="1">Đồ uống</option>
                </select>
                <br />
                <button type="submit" class="btn btn-primary">Thêm sản phẩm</button>
            </div>
        </form>
    </div>
</div>

<div id="popup2" class="overlay">
    <div class="popup">
        <h2>Sửa sản phẩm</h2>
        <a class="close" href="#">&times;</a>
        <form action="{{route('admin.edit-product')}}" method="post">
            @csrf
            <div class="mt-4">
                <input type="hidden" name="id" id="id_sp"/>
                <input class="form-control" name="ten_sp" placeholder="Ten san pham" id="input-name-sp">
                <div><br /></div>
                <select class="form-select" name="loai_sp" id='select-lsp'>
                    <option value="0">Đồ ăn</option>
                    <option value="1">Đồ uống</option>
                </select>
                <br />
                <button type="submit" class="btn btn-primary">Sửa sản phẩm</button>
            </div>
        </form>
    </div>
</div>
@endsection
@section('script')

<script>
    // $('.import-product').addClass('active');

    function format(number) {
        return number.toLocaleString('en-US');
    }

    $('.format-price').each(function() {
        let format = $.number($(this).text(), {
            style: "decimal"
        });

        $(this).text(format);
    })

    $('.edit').click(function() {
        let sp =JSON.parse($(this).attr('sp'));
        
        $('#id_sp').val(sp.id);
        $('#input-name-sp').val(sp.ten_san_pham);
        $('#select-lsp').val(sp.loai_san_pham)
    })
</script>
@endsection