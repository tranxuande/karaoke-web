@extends('admin.layout.main')
@section('style')
<style>
    .content {
        width: 90%;
        margin: 50px auto;
    }
</style>
@endsection
@section('content')
<div class="conatiner-fluid content">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    <div class="header-title">
                        <h4 class="card-title">Tạo phòng karaoke</h4>
                    </div>
                </div>
                <div class="card-body">
                    <form class="row g-3 needs-validation" id="myform" novalidate action="{{ route('admin.create-room') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="col-md-6">
                            <label for="validationCustom03" class="form-label">Chọn ảnh</label>
                            <input type="file" name="file" class="form-control" aria-label="file example" required onchange="loadFile(event)">
                            <img id="output" height="210px" width="280px" class="mt-3 mb-3" src="../images.png" />
                        </div>
                        <div class="col-md-6">
                            <div class="mb-5">
                                <label for="validationCustom02" class="form-label">Tên phòng</label>
                                <input type="text" class="form-control" id="validationCustom02" name="ten_phong" required>
                            </div>
                            <div class="mb-5">
                                <label for="validationCustom02" class="form-label">Giá 1 giờ (vnd)</label>
                                <input type="text" name="gia" class="form-control" id="number-input" required >
                            </div>
                            <div class="mb-5">
                                <label for="validationCustom02" class="form-label">Số lượng người</label>
                                <input type="text" name="so_luong_nguoi" class="form-control" id="number-input" required >
                            </div>
                            <div>
                                <label for="validationCustom02" class="form-label">Loại phòng</label>
                                <select class="form-select" id="validationCustom04" required name="loai_phong">
                                    <option selected disabled value="">Choose...</option>
                                    @foreach($listKindRoom as $i)
                                    <option value="{{$i->id}}">{{$i->ten_loai_phong}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-12">
                            <button class="btn btn-primary" type="submit">Tạo phòng</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('script')
<script>
    var loadFile = function(event) {
        var output = document.getElementById('output');
        output.src = URL.createObjectURL(event.target.files[0]);
        output.onload = function() {
            URL.revokeObjectURL(output.src)
        }
    };

    const numberInput = document.getElementById('number-input');

    numberInput.addEventListener('keypress', function(event) {
        const keyCode = event.keyCode || event.which;
        const keyValue = String.fromCharCode(keyCode);

        // Chỉ cho phép nhập số và dấu chấm
        const regex = /^[0-9.]+$/;
        if (!regex.test(keyValue)) {
            event.preventDefault();
        }
    });

    numberInput.addEventListener('keyup', function(event) {
        const value = event.target.value;

        // Xóa tất cả ký tự không phải số hoặc dấu chấm
        const formattedValue = value.replace(/[^0-9.]/g, '');

        // Tạo định dạng có dấu phẩy
        const parts = formattedValue.split('.');
        const integerPart = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',');
        const decimalPart = parts.length > 1 ? '.' + parts[1] : '';
        const formattedNumber = integerPart + decimalPart;

        // Cập nhật giá trị vào ô input
        event.target.value = formattedNumber;
    });

    $('#myform').on('submit', function() {
        var inputVal = $('#number-input').val();
        var cleanedVal = inputVal.replace(/,/g, '');
        $('#number-input').val(cleanedVal);
    });

    $('.list-room').addClass('active');
</script>
@endsection