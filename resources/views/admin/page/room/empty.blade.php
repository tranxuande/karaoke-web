@extends('admin.layout.main')
@section('style')

<style>
    .room-item {
        height: 200px;
        width: 280px;
        margin-right: 20px;
        margin-bottom: 20px;
    }

    .image-room {
        position: relative;
        border-radius: 10px;
    }

    .room-item.full {
        height: 200px;
        width: 280px;
        background-color: #ECECEC;
        border-radius: 10px;
    }

    .hover-card {
        height: 50px;
        width: 280px;
        background-color: white;
        opacity: 0.6;
    }

    .room-content,
    .hover-card {
        position: absolute;

    }


    .hover-card,
    .room-content {
        display: block;
    }

    .full {
        height: 200px;
        width: 280px;
        background-color: #ECECEC;
        border-radius: 10px;
        position: absolute;
        display: none;
        border: 1px solid #003B95;
    }


    .button {
        font-size: 1em;
        padding: 10px;
        color: black;
        border: 2px solid #06D85F;
        border-radius: 10px;
        text-decoration: none;
        cursor: pointer;
        transition: all 0.3s ease-out;
    }

    .button:hover {
        background: #06D85F;
        color: white;
    }

    .overlay {
        position: fixed;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        background: rgba(0, 0, 0, 0.7);
        transition: opacity 500ms;
        visibility: hidden;
        opacity: 0;

    }

    .overlay:target {
        visibility: visible;
        opacity: 1;
    }

    .popup {
        margin: 70px auto;
        padding: 20px;
        background: #fff;
        border-radius: 5px;
        width: 50%;
        position: relative;
    }

    .content {
        height: 500px;

        overflow-x: hidden !important;
    }

    .popup h2 {
        margin-top: 0;
        color: #333;
        font-family: Tahoma, Arial, sans-serif;
    }

    .popup .close {
        position: absolute;
        top: 20px;
        right: 30px;
        transition: all 200ms;
        font-size: 30px;
        font-weight: bold;
        text-decoration: none;
        color: #333;
    }

    .popup .close:hover {
        color: #06D85F;
    }

    .popup .content {
        max-height: 30%;
        overflow: auto;
    }


    td,
    th {
        border: 1px solid;
    }

    #table-bill {
        width: 90%;
        margin: 0 auto;
    }

    /*.no-border {*/
    /*    border: none;*/
    /*}*/

    .right {
        text-align: right;
    }

    .row {
        margin-bottom: 20px;
        font-size: 20px;
        color: black;
    }
</style>
@endsection
@section('content')
<div class=" bg-white px-5 py-5 ">
    @foreach($loaiPhong as $lp)
    <h2>{{$lp->ten_loai_phong}}</h2>

    <div class="room d-flex flex-wrap">
        @foreach($phong as $p)
        @if($p->id_loai_phong==$lp->id)
        <div class="room-item d-flex" id="room{{$p->id}}" id_r='{{$p->id}}'>
            <img src="{{asset($p->image)}}" alt="error" class="image-room w-100 h-100">
            <div class="hover-card ">

            </div>
            <div class="room-content">
                <h5>{{$p->ten_phong}}</h5>
                
            </div>

            <div class="full">
                <div class="d-flex flex-column justify-content-center align-items-center w-100 h-100">
                    <h5>{{$p->ten_phong}}</h5>
                    <div class="booked">abc</div>
                    <div id="bill-time" style="color: red"></div>
                    <div>
                        <button class="btn btn-info start-room">Start</button>
                        <button class="btn btn-info cancel-room">Cancel</button>
                        <button class="btn btn-info end-room">End Room</button>
                    </div>
                </div>

            </div>
        </div>
        @endif
        @endforeach

    </div>
    @endforeach
</div>
<div id="popup1" class="overlay">
    <div class="popup">
        <h2>Hoa don <span id="bill-tp">303</span></h2>
        <a class="close" href="#">&times;</a>
        <div class="content container mt-4">
            <div class="row ">
                <div class="col-4 text-center ">Gio bat dau</div>
                <div class="col-8 bill-gbd ">11;11;11</div>
            </div>

            <div class="row">
                <div class="col-6 text-center">Gia gio: <span id="bill-g1g">10</span></div>
            </div>
            <div class="row">
                <div class="col-12">
                    <table id="table-bill">
                        <tr>
                            <th>Ten</th>
                            <th>so luong</th>
                            <th>gia</th>
                            <th>tong</th>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script src="https://js.pusher.com/4.1/pusher.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/file-saver@2.0.5/dist/FileSaver.min.js"></script>

<script>
    $('.end-room').css('display', 'none')


    $(document).ready(function() {
        // Khởi tạo một đối tượng Pusher với app_key
        var pusher = new Pusher('8fe09263428456adde30', {
            cluster: 'ap1',
            encrypted: true
        });

        //Đăng ký với kênh chanel-demo-real-time mà ta đã tạo trong file DemoPusherEvent.php
        var channel = pusher.subscribe('channel-demo-real-time');

        //Bind một function addMesagePusher với sự kiện DemoPusherEvent
        channel.bind('App\\Events\\PusherRoom', function(data) {
            let id = data.message.idR;
            $('#room' + id).find('.full').css("display", "block");
            $('#room' + id).attr('id_hd', data.message.idHd);


            if (data.message.statusRoom == 2) {
                $('#room' + id).find('.view-bill').css('display', 'none');
                $('#room' + id).find('.booked').text('Phòng đã đặt');
                let chanel1 = pusher.subscribe('room-rented');
                chanel1.bind('App\\Events\\RoomRented', function(data) {
                    $('#room' + data.room).find('#bill-time').text(data.time);
                    checkRoomsTime(id, $('#room' + data.room).find('#bill-time').text());
                });
            }

            // $('#room' + id).find('.end-room').attr('url', '/admin/hoadon/' + data.message.idHd);
        });


    });

    $('.start-room').on('click', function() {
        $(this).css('display', 'none');
        $(this).siblings('.cancel-room').hide();
        $(this).siblings('.end-room').show();
        let idHd = $(this).closest('.room-item').attr('id_Hd');
        let idR = $(this).closest('.room-item').attr('id_r');
        $(this).closest('.room-item').find('.booked').hide();
        $.ajax({
            type: 'post',
            url: 'start-room/' + idHd,
            success: function(result) {
                realTime(idR, result.hd.thoi_gian_vao);
            },
            error: function() {
                console.log('err')
            }
        });
    })

    $('.end-room').click(function() {
        // let url = $(this).attr('url');
        // window.location = url;
        let idHd = $(this).closest('.room-item').attr('id_Hd');
        let id = $(this).closest('.room-item').attr('id_r');
        $.ajax({
            type: 'get',
            url: 'hoadon/' + idHd,
            xhrFields: {
                responseType: 'blob'
            },
            success: function(data, textStatus, request) {
                // var filename = "";
                // var disposition = request.getResponseHeader('Content-Disposition');
                // if (disposition && disposition.indexOf('attachment') !== -1) {
                //     var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                //     var matches = filenameRegex.exec(disposition);
                //     if (matches != null && matches[1]) {
                //         filename = matches[1].replace(/['"]/g, '');
                //     }
                // }
                // var blob = new Blob([data], {
                //     type: 'application/pdf'
                // });
                // saveAs(blob, filename);
                var blob = new Blob([data], {
                    type: 'application/pdf'
                });
                var filename = "";
                var disposition = request.getResponseHeader('Content-Disposition');
                if (disposition && disposition.indexOf('attachment') !== -1) {
                    var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                    var matches = filenameRegex.exec(disposition);
                    if (matches != null && matches[1]) {
                        filename = decodeURIComponent(matches[1].replace(/['"]/g, ''));
                    }
                }
                saveAs(blob, filename);
                $('#room' + id).find('.full').css("display", "none");
                $('#room' + id).removeAttr('id_hd');
            },
            error: function() {

            }
        })
    })

    function realTime(room, time) {
        let rentedAt = new Date(time);
        setInterval(function() {
            let now = new Date();
            let diff = now - rentedAt;
            let hours = Math.floor(diff / (1000 * 60 * 60));
            let minutes = Math.floor((diff / (1000 * 60)) % 60);
            let seconds = Math.floor((diff / 1000) % 60);
            $('#room' + room).find('#bill-time').text(hours + ':' + minutes + ':' + seconds);
        }, 1000);
    }


    function checkRoomsTime(idr, time) {
        let intervalId = setInterval(function() {
            let currentTime = new Date();
            let targetTime = new Date();
            let hour = $('#room' + idr).find('#bill-time').text().split(":");
            targetTime.setHours(parseInt(hour[0])); // Đặt giờ
            targetTime.setMinutes(parseInt(hour[1])); // Đặt phút

            if (currentTime.getHours() === targetTime.getHours() && currentTime.getMinutes() === targetTime.getMinutes()) {          
                let idHd = $('#room' + idr).attr('id_hd');
                $.ajax({
                    method: 'post',
                    url: 'cancel/' + idHd,
                    success: function(result) {
                        $('#room' + idr).find('.full').css('display', 'none');
                    },
                    error: function() {
                        console.log('errr');
                    }
                })
                clearInterval(intervalId);
            }
        }, 1000);
    }


    $('.empty-room').addClass('active');

    let listRoom = @json($phong);

    for (let i in listRoom) {
        if (listRoom[i].tinh_trang == 1) {
            $('#room' + listRoom[i].id).find('.full').css('display', 'block');

            $.ajax({
                type: 'post',
                url: 'find/' + listRoom[i].id,
                success: function(result) {
                    console.log(result)
                    // $('#view' + listRoom[i].id).attr('id_hd', result.hd.id);
                    $('#room' + listRoom[i].id).attr('id_hd', result.hd.id);
                    $('#room' + listRoom[i].id).find('.start-room').hide();
                    $('#room' + listRoom[i].id).find('.cancel-room').hide();
                    $('#room' + listRoom[i].id).find('.booked').hide();
                    $('#room' + listRoom[i].id).find('.end-room').show();
                    $('#room' + listRoom[i].id).find('.end-room').attr('url', '/admin/hoadon/' + result.hd.id);
                    realTime(listRoom[i].id, result.hd.thoi_gian_vao);
                },
                error: function() {
                    console.log('err')
                }
            });
        } else if (listRoom[i].tinh_trang == 2) {
            $('#room' + listRoom[i].id).find('.full').css('display', 'block');
            $.ajax({
                type: 'post',
                url: 'find/' + listRoom[i].id,
                success: function(result) {

                    $('#room' + listRoom[i].id).find('.booked').text('Phòng đã đặt');
                    $('#room' + listRoom[i].id).find('#bill-time').text(result.hd.thoi_gian_dat.substring(0, 5));
                    $('#room' + listRoom[i].id).attr('id_hd', result.hd.id);
                    $('#room' + listRoom[i].id).find('.end-room').attr('url', '/admin/hoadon/' + result.hd.id);
                    
                    checkRoomsTime(listRoom[i].id,result.hd.thoi_gian_dat)
                },
                error: function() {
                    console.log('err')
                }
            });
            $('#room' + listRoom[i].id).find('.view-bill').replaceWith('<div class="booked">Phòng đã đặt</div>');
        }
    }

    $('.button').click(function() {
        $('.del-f').remove();
        let idHd = $(this).attr('id_hd');

        $.ajax({
            type: 'get',
            url: '/getBill/' + idHd,
            success: function(result) {
                let hdBill = result.hdBill;
                let listSpBill = result.listSpBill;
                $('#bill-tp').text(hdBill.ten_phong);
                $('.bill-gbd').text(hdBill.thoi_gian_vao);
                $('#bill-g1g').text(hdBill.gia_1_gio);

                for (let i in listSpBill) {
                    let tt = listSpBill[i].so_luong_ban * listSpBill[i].gia_ban;
                    // let $trTdv = $("#");
                    // let $trNew = $("<tr class='del-f'>").html(
                    //     `
                    //         <td>${listSpBill[i].ten_san_pham}</td>
                    //         <td>${listSpBill[i].so_luong_ban}</td>
                    //         <td>${listSpBill[i].gia_ban}</td>
                    //         <td class="right tongtien">${tt}</td>
                    //     `
                    // );
                    // $trTdv.before($trNew);

                    $('#table-bill').append(`
                            <tr class='del-f'>
                                <td>${listSpBill[i].ten_san_pham}</td>
                                <td>${listSpBill[i].so_luong_ban}</td>
                                <td>${listSpBill[i].gia_ban}</td>
                                <td class="right tongtien">${tt}</td>
                            </tr
                        `)
                }
            },
            error: function() {
                console.log('err')
            }
        });
    })

    $('.cancel-room').click(function() {
        let idR=$(this).closest('.room-item').attr('id_r');
        let idHd = $(this).closest('.room-item').attr('id_hd');
        $.ajax({
            type: 'post',
            url: 'cancel/'+idHd,
            success: function(data) {
                $('#room' + idR).find('.full').css('display', 'none');
            },
            error: function() {
                console.log('errr');
            }
        })
    })
</script>
@endsection