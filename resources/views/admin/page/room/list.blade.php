@extends('admin.layout.main')
@section('style')
<style>
    .icon-32 {
        cursor: pointer;
    }

    #paginate {
        margin: 20px auto;
    }
</style>
@endsection
@section('content')

<div class="conatiner-fluid  ">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">

                @if (session('mess'))
                <div class="alert alert-left alert-success alert-dismissible fade show mb-3" role="alert">
                    <span> {{session('mess')}}</span>
                    <button type="button" class="btn-close btn-close-white" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
                @endif

                <div class="card-header d-flex justify-content-between">
                    <div class="header-title">
                        <h4 class="card-title">Danh sách phòng karaoke</h4>
                    </div>
                    <a href="{{route('admin.create-room')}}">
                        <button type="button" class="btn btn-primary"> <svg class="icon-32" width="32" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M12.0001 8.32739V15.6537" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                <path d="M15.6668 11.9904H8.3335" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M16.6857 2H7.31429C4.04762 2 2 4.31208 2 7.58516V16.4148C2 19.6879 4.0381 22 7.31429 22H16.6857C19.9619 22 22 19.6879 22 16.4148V7.58516C22 4.31208 19.9619 2 16.6857 2Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                            </svg> Thêm phòng</button>
                    </a>
                </div>
                <div class="card-body p-0">
                    <div class="table-responsive mt-4">
                        <table id="basic-table" class="table table-striped mb-0" role="grid">
                            <thead>
                                <tr>
                                    <th>Image</th>
                                    <th>Tên phòng</th>
                                    <th>Giá 1 giờ</th>
                                    <th>Loại phòng</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($listRoom as $i)
                                <tr id='{{$i->id_phong}}' </tr>
                                    <td>
                                        <div class="d-flex align-items-center">
                                            <img class="rounded " height="150" width="200px" src="{{asset($i->image)}}" alt="profile">
                                        </div>
                                    </td>
                                    <td>
                                        {{$i->ten_phong}}
                                    </td>
                                    <td class="format-price">{{$i->gia_1_gio}}</td>
                                    <td>
                                        @switch($i->id_loai_phong)
                                        @case(1)
                                        <div class="badge rounded-pill bg-secondary">{{$i->ten_loai_phong}}</div>
                                        @break

                                        @case(2)
                                        <div class="badge rounded-pill bg-warning  ">{{$i->ten_loai_phong}}</div>
                                        @break

                                        @case(3)
                                        <div class="badge bg-info ">{{$i->ten_loai_phong}}</div>
                                        @break

                                        @case(4)
                                        <div class="badge bg-success">{{$i->ten_loai_phong}}</div>
                                        @break

                                        @default
                                        <div class="">{{$i->ten_loai_phong}}</div>
                                        @endswitch

                                    </td>
                                    <td>
                                        <a href="{{route('admin.edit-room',$i->id_phong)}}">
                                            <svg class="icon-32 mr-3" width="" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd" clip-rule="evenodd" d="M16.6653 2.01034C18.1038 1.92043 19.5224 2.41991 20.5913 3.3989C21.5703 4.46779 22.0697 5.88633 21.9898 7.33483V16.6652C22.0797 18.1137 21.5703 19.5322 20.6013 20.6011C19.5323 21.5801 18.1038 22.0796 16.6653 21.9897H7.33487C5.88636 22.0796 4.46781 21.5801 3.39891 20.6011C2.41991 19.5322 1.92043 18.1137 2.01034 16.6652V7.33483C1.92043 5.88633 2.41991 4.46779 3.39891 3.3989C4.46781 2.41991 5.88636 1.92043 7.33487 2.01034H16.6653ZM10.9811 16.845L17.7042 10.102C18.3136 9.4826 18.3136 8.48364 17.7042 7.87427L16.4056 6.57561C15.7862 5.95625 14.7872 5.95625 14.1679 6.57561L13.4985 7.25491C13.3986 7.35481 13.3986 7.52463 13.4985 7.62453C13.4985 7.62453 15.0869 9.20289 15.1169 9.24285C15.2268 9.36273 15.2967 9.52256 15.2967 9.70238C15.2967 10.062 15.007 10.3617 14.6374 10.3617C14.4675 10.3617 14.3077 10.2918 14.1978 10.1819L12.5295 8.5236C12.4496 8.44368 12.3098 8.44368 12.2298 8.5236L7.46474 13.2887C7.13507 13.6183 6.94527 14.0579 6.93528 14.5274L6.87534 16.8949C6.87534 17.0248 6.9153 17.1447 7.00521 17.2346C7.09512 17.3245 7.21499 17.3744 7.34486 17.3744H9.69245C10.172 17.3744 10.6315 17.1846 10.9811 16.845Z" fill="currentColor"></path>
                                            </svg>
                                        </a>
                                        <span class="delete" idr='{{$i->id_phong}}' style="color: #FF0033">
                                            <svg class="icon-32" width="" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd" clip-rule="evenodd" d="M20.2871 5.24297C20.6761 5.24297 21 5.56596 21 5.97696V6.35696C21 6.75795 20.6761 7.09095 20.2871 7.09095H3.71385C3.32386 7.09095 3 6.75795 3 6.35696V5.97696C3 5.56596 3.32386 5.24297 3.71385 5.24297H6.62957C7.22185 5.24297 7.7373 4.82197 7.87054 4.22798L8.02323 3.54598C8.26054 2.61699 9.0415 2 9.93527 2H14.0647C14.9488 2 15.7385 2.61699 15.967 3.49699L16.1304 4.22698C16.2627 4.82197 16.7781 5.24297 17.3714 5.24297H20.2871ZM18.8058 19.134C19.1102 16.2971 19.6432 9.55712 19.6432 9.48913C19.6626 9.28313 19.5955 9.08813 19.4623 8.93113C19.3193 8.78413 19.1384 8.69713 18.9391 8.69713H5.06852C4.86818 8.69713 4.67756 8.78413 4.54529 8.93113C4.41108 9.08813 4.34494 9.28313 4.35467 9.48913C4.35646 9.50162 4.37558 9.73903 4.40755 10.1359C4.54958 11.8992 4.94517 16.8102 5.20079 19.134C5.38168 20.846 6.50498 21.922 8.13206 21.961C9.38763 21.99 10.6811 22 12.0038 22C13.2496 22 14.5149 21.99 15.8094 21.961C17.4929 21.932 18.6152 20.875 18.8058 19.134Z" fill="currentColor"></path>
                                            </svg>
                                        </span>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div id="paginate">
                    {{ $listRoom->appends(request()->all())->links() }}
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
@section('script')

<script>
    $('.delete').click(function() {
        let id = $(this).attr('idr');
        Swal.fire({
            title: 'Xác nhận xóa phòng?',
            text: "Bạn có chắc muốn xóa phòng này!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Hủy bỏ',
            confirmButtonText: 'Vâng, xóa phòng!'
        }).then((result) => {
            if (result.isConfirmed) {
                $.post('delete-room/' + id, function(data) {
                    if (data.status == 200) {
                        $('#' + id).remove();
                        Swal.fire({
                            icon: 'success',
                            title: 'Xoá phòng thành công',
                        });
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'Something went wrong!',
                        });
                    }
                }, 'json').fail(function(xhr, status, error) {
                    // Xử lý lỗi
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Something went wrong!',
                    });

                });
            }
        });
    });

    $('.list-room').addClass('active');
    $('.home').removeClass('active');

    function format(number) {
        return number.toLocaleString('en-US');
    }

    $('.format-price').each(function() {
        let format = $.number($(this).text(), {
            style: "decimal"
        });

        $(this).text(format);
    })
</script>
@endsection