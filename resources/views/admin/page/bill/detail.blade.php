@extends('admin.layout.main')
@section('style')
<style>
    td,
    th {
        border: 1px solid;
    }

    #table-bill {
        width: 700px;
        margin: 0 auto;
    }

    .no-border {
        border: none;
    }

    .right {
        text-align: right;
    }

    .row {
        margin-bottom: 20px;
        font-size: 20px;
        color: black;
    }
</style>
@endsection
@section('content')
<div class=" container bill card py-3">
    <div class="row">
        <div class="col-12 text-center">
            <h3>Hoa don {{$phong->ten_phong}}</h3>
        </div>
    </div>
    <div class="row ">
        <div class="col-4 text-center ">Gio bat dau</div>
        <div class="col-8 ">{{$hd->thoi_gian_vao}}</div>
    </div>
    <div class="row">
        <div class="col-4 text-center">Gio ket thuc</div>
        <div class="col-8 ">{{$hd->thoi_gian_ra}}</div>
    </div>
    <div class="row">
        <div class="col-6 text-center">Gia gio: {{$phong->gia_1_gio}}</div>
    </div>
    <div class="row">
        <div class="col-12">
            <table id="table-bill">
                <tr>
                    <th>Ten</th>
                    <th>so luong</th>
                    <th>gia</th>
                    <th>tong</th>
                </tr>

                @foreach($listSpBill as $i)
                <tr>
                    <td>{{$i->ten_san_pham}}</td>
                    <td>{{$i->so_luong_ban}}</td>
                    <td>{{$i->gia_ban}}</td>
                    <td class="right tongtien">{{$i->so_luong_ban*$i->gia_ban}}</td>
                </tr>
                @endforeach
                <tr>
                    <td colspan="3" class="no-border">Tong dich vu</td>
                    <td class="no-border right tongDv">1000000</td>
                </tr>
                <tr>
                    <td colspan="3" class="no-border">Tong tien gio</td>
                    <td class="no-border right tongTp">{{$tongTienPhong}}</td>
                </tr>
                <tr>
                    <td colspan="3" class="no-border">Tong</td>
                    <td class="no-border right total"></td>
                </tr>
            </table>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    let sum = 0;
    $('.tongtien').each(function() {
        sum += parseInt($(this).text());
    })
    $('.tongDv').text(sum);
    $('.total').text(parseInt($('.tongDv').text()) + parseInt($('.tongTp').text()));
</script>
@endsection