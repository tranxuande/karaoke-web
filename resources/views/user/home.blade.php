@extends('user.main')
@section('style')
<style>
    .header {
        background-image: url("{{asset('assets/images/Rectangle 1969.png')}}");
        background-repeat: no-repeat;
        height: 70px;
    }

    .body {
        width: 950px;
        margin: 50px auto;
        text-align: center;
    }

    .account,
    .account h4 {
        color: white;
    }

    .icon {
        font-size: 30px;
        margin-right: 20px;
    }

    .account {
        margin-right: 100px;
    }

    .room-item {
        height: 300px;
        width: 450px;
        /* margin-right: 20px; */
        margin-bottom: 40px;
    }

    .image-room {
        position: relative;
        border-radius: 10px;
    }

    /* .room-item.full {
        height: 200px;
        width: 280px;
        background-color: #ECECEC;
        border-radius: 10px;
    } */

    .hover-card {
        height: 70px;
        width: 450px;
        background-color: white;
        opacity: 0.6;
    }

    .room-content,
    .hover-card {
        position: absolute;

    }

    .room-content {
        width: 450px;
        padding: 10px 15px 0 15px;
    }

    .hover-card,
    .room-content {
        display: block;
    }

    .full {
        height: 300px;
        width: 450px;
        background-color: #ECECEC;
        border-radius: 10px;
        position: absolute;
        display: none;
        border: 1px solid #003B95;
    }

    .body h2 {
        color: #003B95;
    }

    .choose a {
        padding: 10px;
        background-color: #003B95;
        color: white;
        border: none;
        border-radius: 20px;
    }

    .choose a {
        /* margin-top: 5px; */
        display: none;
    }

    .choose p {
        color: #003B95;
        font-size: 30px;

    }

    .room-item:hover .choose>a {
        display: block;
    }

    .room-item:hover .choose>p {
        display: none;
    }
</style>
@endsection
@section('content')
<div class="header d-flex justify-content-between align-items-center">
    <div></div>
    <div class="account d-flex align-items-center ">
        <i class="fa-solid fa-circle-user icon "></i>
        <div>
            <h4>Xin chào</h4>
        </div>
    </div>
</div>

<div class="body">
    @foreach($loaiPhong as $lp)
    <h2 class="mb-5">{{$lp->ten_loai_phong}}</h2>
    <div class="room d-flex flex-wrap justify-content-between">
        @foreach($phong as $p)
        @if($p->id_loai_phong==$lp->id)
        <div class="room-item d-flex" id="room{{$p->id}}">
            <img src="{{asset($p->image)}}" alt="error" class="image-room w-100 h-100">
            <div class="hover-card ">

            </div>
            <div class="room-content d-flex justify-content-between">
                <div>
                    <h5 style="color:#003B95">{{$p->ten_phong}}</h5>
                    <p style="color:black">Số người: {{$p->so_luong_nguoi}}</p>
                </div>
                <div class="choose">
                    <p><span>{{$p->gia_1_gio}}</span>đ/h</p>
                    <a href="{{route('detail',$p->id)}}" style="color: white">Chọn phòng</a>
                </div>
            </div>

            <div class="full">
                <div class="d-flex flex-column justify-content-center align-items-center w-100 h-100">
                    <h3 style="margin-bottom:10px">{{$p->ten_phong}}</h3>
                    <h3 style="color: #003B95;">Đã đầy</h3>
                </div>
            </div>
        </div>
        @endif
        @endforeach
    </div>
    <div class="mb-5"></div>
    @endforeach
</div>
@endsection
@section('script')
<script>
    $(document).ready(function() {
        // Khởi tạo một đối tượng Pusher với app_key
        var pusher = new Pusher('8fe09263428456adde30', {
            cluster: 'ap1',
            encrypted: true
        });

        //Đăng ký với kênh chanel-demo-real-time mà ta đã tạo trong file DemoPusherEvent.php
        var channel21 = pusher.subscribe('channel-demo-real-time');

        //Bind một function addMesagePusher với sự kiện DemoPusherEvent
        channel21.bind('App\\Events\\PusherRoom', function(data) {
            let id = data.message.idR;
            $('#room' + id).find('.full').css("display", "none");

        });


        var channel22 = pusher.subscribe('channel-demo-real-time');

        //Bind một function addMesagePusher với sự kiện DemoPusherEvent
        channel21.bind('App\\Events\\PusherRoom', function(data) {
            let id = data.message.idR;
            $('#room' + id).find('.full').css("display", "none");

        });

    });

    let phong = @json($phong);

    for (let i in phong) {
        if (phong[i].tinh_trang == 1 || phong[i].tinh_trang == 2) {
            $('#room' + phong[i].id).find('.full').css('display', 'block');
        }
    }

    $('.choose p span').each(function() {
        let format = $.number($(this).text(), {
            style: "decimal"
        });
        $(this).text(format);
    });
</script>
@endsection