@extends('user.main')
@section('style')
<style>
    .view {
        background-image: url("{{asset('assets/images/top.jpg')}}");
        background-repeat: no-repeat;
        height: 750px;
        background-size: 100% 100%;
    }

    .bill {
        width: 700px;
        max-height: 600px;
        overflow-y: auto;
        overflow-x: hidden;
    }


    td,
    th {
        border: 1px solid;
    }

    #table-bill {
        width: 600px;
        margin: 0 auto;
    }

    .no-border {
        border: none;
    }

    .right {
        text-align: right;
    }

    .row {
        margin-bottom: 20px;
        font-size: 20px;
        color: black;
    }

    .text{
        padding-left: 100px;
    }
</style>
@endsection
@section('content')
<div class="container-fuild view d-flex justify-content-center align-items-center">
    <div class="bill card py-3">
        <div class="row">
            <div class="col-12 text-center">
                <h3>Hóa đơn {{$phong->ten_phong}}</h3>
            </div>
        </div>
        <div class="row ">
            <div class="col-4 text ">Giờ bắt đầu:</div>
            <div class="col-5 ">{{session('hd')['thoi_gian_vao']}}</div>
        </div>
        <div class="row">
            <div class="col-4 text">Giá giờ:</div>
            <div class="col-2 format-price">{{$phong->gia_1_gio}}</div>
        </div>
        <div class="row">
            <div class="col-12">
                <table id="table-bill">
                    <tr>
                        <th>Tên</th>
                        <th>Số lượng</th>
                        <th>Giá</th>
                        <th>Tổng tiền</th>
                    </tr>

                    @foreach(session('foods') as $i => $val)
                    <tr>
                        <td>{{session('foods')[$i]}}</td>
                        <td class="text-center">{{session('sls')[$i]}}</td>
                        <td class="format-price">{{session('moneys')[$i]}}</td>
                        <td class="right tongtien format-price">{{session('tongTien')[$i]}}</td>
                    </tr>
                    @endforeach
                    <tr>
                        <td colspan="3" class="no-border">Tổng dịch vụ</td>
                        <td class="no-border right tongDv format-price">1000000</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
    let t = 0;
    $('.tongtien').each(function() {
        t += (Number)($(this).text());
    })
    $('.tongDv').text(t);

    $('.format-price').each(function() {
        let format = $.number($(this).text(), {
            style: "decimal"
        });

        $(this).text(format);
    })

</script>
@endsection