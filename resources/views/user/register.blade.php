@extends('user.main')
@section('style')
<style>
    .view {
        background-image: url("{{asset('assets/images/image 30.png')}}");
        background-repeat: no-repeat;
        height: 750px;
    }

    .logo {
        position: relative;
    }

    .image-sub {
        position: absolute;
        left: 40%;
    }

    .image-music {
        position: absolute;
        left: 0;
        top: 100%;
    }

    .register-content {
        background-color: rgba(255, 255, 255, 0);
        width: 500px;
        padding: 10px;
        border: 1px solid white;
    }

    form {
        width: 390px;
        margin: 0 auto;
    }

    .register-content .input {
        border: none;
        border-radius: 60px;
        width: 390px;
        height: 50px;
    }

    .html{
        font-size: 30px;
        height: 20px;
        width: 20px;
    }
    .gt{
        font-size: 25px;
        color: white;
    }
    #submit{
        background-color: #003B95;
        color: white;
    }
</style>
@section('content')
<div class="container-fuild view d-flex flex-column justify-content-center align-items-center">
    <div class="logo ">
        <img src="{{asset('assets/images/Vector.png')}}" alt="" class="image-main">
        <img src="{{asset('assets/images/Vector (1).png')}}" alt="" class="image-sub">
        <img src="{{asset('assets/images/Vector (2).png')}}" alt="" class="image-music">
    </div>
    <div class="mt-5  register-content">
        <form action="{{route('register')}}" method="post">
            @csrf
            <div class="row mb-3 ">
                <input  name="name" class="input" placeholder="Ten khach hang"/>
            </div>
            <div class="row mb-3 ">
                <input  name="sdt" class="input" placeholder="so dien thoai"/>
            </div>
            <div class="row mb-3 ">
                <input  name="email" class="input" placeholder="Email"/>
            </div>
            <div class="row mb-3 ">
                <div class="col-6">
                    <input type="radio" class="html" name="gt" value="0">
                    <label for="html" class="gt">Nam</label><br>
                </div>
                <div class="col-6">
                    <input type="radio" class="html" name="gt" value="1">
                    <label for="html" class="gt">Nu</label><br>
                </div>

            </div>
            <div class="row">
                <input type="submit" value="Submit" id="submit" class="input" />
            </div>
        </form>
    </div>
</div>
@endsection