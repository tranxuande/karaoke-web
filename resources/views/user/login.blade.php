@extends('user.main')
@section('style')
<style>
    .view {
        background-image: url("{{asset('assets/images/image 30.png')}}");
        background-repeat: no-repeat;
        height: 750px;
    }

    .logo {
        position: relative;
    }

    .image-sub {
        position: absolute;
        left: 40%;
    }

    .image-music {
        position: absolute;
        left: 0;
        top: 100%;
    }

    .login-content h3 {
        color: white;
        font-size: 40px;
    }

    .login-content input {
        border: none;
        border-radius: 60px;
        width: 397.75px;
        height: 56px;
    }

    .login-content #sdt {
        background-color: rgba(255, 255, 255, 0);
        border: 1px solid white;
        color: white;
    }

    .login-content #submit {
        font-size: 25px;
        color: #003B95;
    }
</style>
@endsection
@section('content')
<div class="container-fuild view d-flex flex-column justify-content-center align-items-center">
    <div class="logo ">
        <img src="{{asset('assets/images/Vector.png')}}" alt="" class="image-main">
        <img src="{{asset('assets/images/Vector (1).png')}}" alt="" class="image-sub">
        <img src="{{asset('assets/images/Vector (2).png')}}" alt="" class="image-music">
    </div>
    <div class="mt-5 text-center login-content">
        <h3 class="my-5">LOGIN</h3>
        <form action="{{route('login')}}" method="post">
            @csrf
            <div class="row mb-3">
                <input id="sdt" name="sdt" />
            </div>
            <div class="row">
                <input type="submit" value="Login" id="submit" />
            </div>
        </form>
    </div>
</div>
@endsection