@extends('user.main')
@section('style')
<style>
    .detail {
        margin: 0 auto;
        height: 750px;
        background-image: url("{{asset('assets/images/download (1).jpg')}}");
        background-size: 100% 100%;
        background-repeat: no-repeat;
    }

    ul {
        list-style: none;
        margin-top: 20px;
        padding: 0;
    }

    li {
        cursor: pointer;
        display: flex;
        padding: 0 20px;
        justify-content: space-between;
        margin-bottom: 5px;
        align-items: center;
        width: 100%;
    }

    .menu {
        background-color: white;
    }

    .total input {
        height: 30px;
        width: 70px;
    }

    .card {
        height: 500px;
        overflow: auto;
        margin-left: 20px;
    }

    .total {
        height: 80%;
        overflow: auto;
    }

    .order {
        background-color: #6699CC;
        color: white;
        border-radius: 20px;
        width: 100%;
    }

    .order-item:hover {
        background-color: #6699CC;
        color: white;
        border-radius: 20px;
    }

    .order-item:hover h5 {
        color: white;
    }

    .order h5 {
        color: white;
    }

    .list-order {
        border-bottom: 1px solid rebeccapurple;
    }

    .trash {
        color: red;
    }

    form {
        height: 100%;
    }

    input.input-border {
        border: none;
        width: 150px;
    }
</style>
@endsection
@section('content')
<div class="detail">
    <div class="container pt-5">
        <h3>{{$room->ten_phong}}</h3>
        <div class="mt-5">
            <div class="row">
                <div class=" col-3 card ">
                    <h4 class="mt-3">Đồ ăn</h4>
                    <ul>
                        @foreach($food as $i)
                        <li id="{{$i->id}}" class="order-item" loai='{{$i->id}}'>
                            <h5>{{$i->ten_san_pham}}</h5>
                            <p class="format-price">{{$i->gia_ban}}</p>
                        </li>
                        @endforeach
                    </ul>
                </div>
                <div class="col-3 card ">
                    <h4 class="mt-3">Đồ uống</h4>
                    <ul>
                        @foreach($drink as $i)
                        <li id="{{$i->id}}" class="order-item" loai='{{$i->id}}'>
                            <h5>{{$i->ten_san_pham}}</h5>
                            <p class="format-price">{{$i->gia_ban}}</p>
                        </li>
                        @endforeach
                    </ul>
                </div>
                <div class="col-5 card ">
                    <form action="{{route('confirm',$room->id)}}" method="post" id="order">
                        @csrf
                        <ul class="total">
                            <li>
                                <h5 class='col-5'>Tên</h5>
                                <h5 class='col-3'>Giá</h5>
                                <h5 class='col-3'>Số lượng</h5>
                                <h5 class="col-1"></h5>
                            </li>
                        </ul>
                        <button class="btn btn-primary" type="submit">Order</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('script')
<script>
    $('li').click(function(e) {
        var className = ($(this).attr('class'));
        if(!className.includes('clicked')){
            let food = $(this).find('h5').text();
            let money = $(this).find('p').text();
            let loai = $(this).attr('loai');

            $(this).addClass('order');
            $(this).addClass('clicked');

            $('.total').append(`
                    <li class="row d-flex align-items-center list-order" >
                        <input type='hidden' name='ids[]' value='${loai}'/>
                        <input name='food[]' value='${food}' class='col-5 input-border' readonly/>
                        <input name='money[]' value='${money}' class='col-5 input-border' readonly/>
                        <input type="text" value="1" name='sl[]' class='col-3'>
                        <i class="fa-sharp fa-solid fa-xmark col-1 trash" onclick="xoa('${loai}',this)"></i>
                    </li>
        `);
        }
    })




    function xoa(loai, ele) {
        let reloai = $('[loai=' + loai + ']');

        reloai.removeClass('order');
        reloai.removeClass('clicked');
        $(ele).closest('li').remove();

    }

    $('.format-price').each(function() {
        let format = $.number($(this).text(), {
            style: "decimal"
        });

        $(this).text(format);
    })

    $('#order').on('submit', function() {
        $( "input[name='money[]']" ).each(function(){
            let inputVal=$(this).val();
            let cleanedVal=inputVal.replace(/,/g, '');
            $(this).val(cleanedVal);
        })
    });
</script>
@endsection
