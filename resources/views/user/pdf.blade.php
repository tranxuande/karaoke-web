@extends('user.main')

@section('style')
<style>
    td,
    th {
        border: 1px solid;
    }

    #table-bill {
        width: 700px;
        margin: 0 auto;
    }

    .no-border {
        border: none;
    }

    .right {
        text-align: right;
    }

    .center {
        text-align: center;
    }

    .row {
        margin-bottom: 20px;
        font-size: 20px;
        color: black;
    }

    .container {
        font-family: DejaVu Sans, sans-serif;
    }
    .border-tong{
        border-top: 1px solid #808080;
    }
</style>
@endsection
@section('content')
<div class=" container bill card py-3">
    <div class="row">
        <div class="col-12 center">
            <h3>Hóa đơn {{$phong->ten_phong}}</h3>
        </div>
    </div>
    <div class="row ">
        <div class="col-4 text-center ">Giờ bắt đầu : {{$hd->thoi_gian_vao}}</div>

    </div>
    <div class="row">
        <div class="col-4 text-center">Giờ kết thúc : {{$hd->thoi_gian_ra}}</div>

    </div>
    <div class="row">
        <div class="col-6 text-center">Giá giờ: {{number_format($phong->gia_1_gio)}}</div>
    </div>
    <div class="row">
        <div class="col-12">
            <table id="table-bill" cellspacing=0>
                <tr>
                    <th>Tên</th>
                    <th>Số lượng</th>
                    <th>Giá</th>
                    <th>Tổng</th>
                </tr>

                @foreach($listSpBill as $i)
                <tr>
                    <td>{{$i->ten_san_pham}}</td>
                    <td class="center">{{$i->so_luong_ban}}</td>
                    <td class="right">{{$i->gia_ban}}</td>
                    <td class="right tongtien">{{number_format($i->so_luong_ban*$i->gia_ban)}}</td>
                </tr>
                @endforeach
                <tr>
                    <td colspan="3" class="no-border">Tổng dịch vụ</td>
                    <td class="no-border right tongDv">{{number_format($tongDv)}}</td>
                </tr>
                <tr>
                    <td colspan="3" class="no-border">Tổng tiền giờ</td>
                    <td class="no-border right tongTp">{{number_format($tongTienPhong)}}</td>
                </tr>
                <tr>
                    <td colspan="3" class="no-border border-tong">Tổng</td>
                    <td class="no-border right total border-tong">{{number_format($tong)}}</td>
                </tr>
            </table>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    // let sum = 0;
    // $('.tongtien').each(function() {
    //     sum += parseInt($(this).text());
    // })
    // $('.tongDv').text(sum);
    // $('.total').text(parseInt($('.tongDv').text()) + parseInt($('.tongTp').text()));
</script>
@endsection