@extends('user.main')
@section('style')
<style>
    .view {
        background-image: url("{{asset('assets/images/top.jpg')}}");
        background-repeat: no-repeat;
        height: 750px;
        background-size: 100% 100%;
    }

    .card {
        padding: 20px;
        width: 600px;
    }
</style>
@endsection
@section('content')
<div class="container-fuild view d-flex justify-content-center align-items-center">
    <div class="card">
        <h2 class="mb-2 text-center">Xác nhận thông tin</h2>
        <p><i><span style="color: red">*</span>Lưu ý: thời gian giữ phòng tối đa là 2 giờ (tính từ thời điểm hiện tại)</i></p>
        <form action="{{route('confirm-hd')}}" method="post" class="needs-validation" novalidate id="form-confirm">
            @csrf
            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="" class="form-label">Ten khach hang</label>
                        <input type="text" class="form-control" name="tkh" placeholder=" " required>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="email" class="form-label">So dien thoai</label>
                        <input type="text" class="form-control" id="number-input" name="sdt" aria-describedby="email" placeholder=" " required>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="email" class="form-label">Email</label>
                        <input type="email" class="form-control" id="email" name="email" aria-describedby="email" placeholder=" " required>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="" class="form-label">Chọn thời gian</label>
                        <input type="time" class="form-control" id="timeInput" name="timeInput" required>
                        <div class="err" style="color: red">

                        </div>
                    </div>
                </div>
            </div>
            <div class="d-flex justify-content-center">
                <button type="submit" class="btn btn-primary datphong">Đặt phòng</button>
            </div>
        </form>
    </div>
</div>
@if (isset($mess))
<div class="success">

</div>
@endif
@endsection
@section('script')
<script>
    if ($('div.success').length > 0 && $('div.success').hasClass('success')) {
        Swal.fire({
            icon: 'success',
            title: 'Đặt phòng thành công',
            showConfirmButton: false,
            footer: '<a href="/">Quay lại trang chủ -></a>'
        })
    }
    const numberInput = $('#number-input');

    numberInput.on('keypress', function(event) {
        const keyCode = event.keyCode || event.which;
        const keyValue = String.fromCharCode(keyCode);

        // Chỉ cho phép nhập số và dấu chấm
        const regex = /^[0-9.]+$/;
        if (!regex.test(keyValue)) {
            event.preventDefault();
        }
    });

    $("#form-confirm").submit(function(event) {
        var now = new Date();
        var selectedTime = new Date(now.toDateString() + ' ' + $('#timeInput').val() + ':00');
        var diffInSeconds = Math.abs(now - selectedTime) / 1000;
        var diffInHours = diffInSeconds / 3600;
        console.log(diffInHours);
        if (diffInHours > 2 && selectedTime < now) {
            $('.err').text('Thời gian chọn không phù hợp');
            event.preventDefault();
        } else if (selectedTime < now) {
            $('.err').text('Thời gian chọn không phù hợp');
            event.preventDefault();
        } else if (diffInHours > 2) {
            $('.err').text('Thời gian chọn không phù hợp');
            event.preventDefault();
        } else {
            $('.err').text('');
        }
    });
</script>
@endsection