<?php

namespace App\Repositories;

use App\Models\SanPham;
use DB;

class SanphamRepository extends BaseRepository
{
    public function getModel()
    {
        return SanPham::class;
    }

    public function findFood($lsp)
    {
        return $this->model->where('loai_san_pham', $lsp)->get();
    }

    public function listSpBill($idHd)
    {
        return $this->model
            ->select('ten_san_pham', 'so_luong_ban', 'gia_ban')
            ->join('hoadon_sanpham', 'san_pham.id', '=', 'hoadon_sanpham.id_sp')
            ->join('hoa_don', 'hoadon_sanpham.id_hoadon', '=', 'hoa_don.id')
            ->where('hoa_don.id', $idHd)
            ->get();
    }

    public function getExSP($food = null)
    {
        return $this->model
            ->where('so_luong_co', '>', 0)
            ->when(isset($food), function ($query) use ($food) {
                return $query->where('loai_san_pham', $food);
            })
            ->get();
    }
}
