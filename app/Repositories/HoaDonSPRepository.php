<?php

namespace App\Repositories;

use App\Models\HoaDonSP;
use DB;

class HoaDonSPRepository extends BaseRepository
{
    public function getModel()
    {
        return HoaDonSP::class;
    }

    public function getTotalByIdSP()
    {
        return
            $this->model
            ->join('san_pham', 'san_pham.id', '=', 'hoadon_sanpham.id_sp')
            ->join('hoa_don', 'hoa_don.id', '=', 'hoadon_sanpham.id_hoadon')
            ->join('phong_karaoke', 'phong_karaoke.id', '=', 'hoa_don.id_phong')
            ->select(DB::raw('SUM(hoadon_sanpham.so_luong_ban*san_pham.gia_ban + TIME_TO_SEC(TIMEDIFF(hoa_don.thoi_gian_ra, hoa_don.thoi_gian_vao)) / 3600 * phong_karaoke.gia_1_gio) as tong_tien'))
            ->groupBy('hoadon_sanpham.id_hoadon')
            ->get();
    }

    public function getByIDHD($idhd)
    {
        return $this->model->where('id_hoadon', $idhd)->get();
    }

    // public function deleteByHd($idhd)
    // {
    //     return $this->model->where('id_hoadon', $idhd)->delete();
    // }
}
