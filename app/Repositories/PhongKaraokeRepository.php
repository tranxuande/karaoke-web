<?php

namespace App\Repositories;

use App\Models\PhongKaraoke;

class PhongKaraokeRepository extends BaseRepository
{
    public function getModel()
    {
        return PhongKaraoke::class;
    }

    public function getListRoom()
    {
        return $this->model
            ->join('loai_phong_karaoke', 'phong_karaoke.id_loai_phong', '=', 'loai_phong_karaoke.id')
            ->select('phong_karaoke.id as id_phong', 'loai_phong_karaoke.id as id_loai_phong', 'image', 'gia_1_gio', 'ten_phong', 'ten_loai_phong')
            ->orderBy('id_phong', 'desc')
            ->paginate(5);
    }

    public function getRoomEmpty()
    {
        return $this->model
            ->where('id_loai_phong', 2)
            ->get();
    }

    public function hdBill($idHd)
    {
        return $this->model
            ->select('ten_phong', 'thoi_gian_vao', 'gia_1_gio')
            ->join('hoa_don', 'hoa_don.id_phong', '=', 'phong_karaoke.id')
            ->where('hoa_don.id', $idHd)
            ->first();
    }

    public function numberRoom($month)
    {
        return $this->model
            ->join('hoa_don', 'phong_karaoke.id', '=', 'hoa_don.id_phong')
            ->join('loai_phong_karaoke', 'loai_phong_karaoke.id', '=', 'phong_karaoke.id_loai_phong')
            ->whereMonth('thoi_gian_vao', '=', $month)
            ->groupBy('loai_phong_karaoke.id')
            ->select('loai_phong_karaoke.id', 'ten_loai_phong', \DB::raw('COUNT(hoa_don.id_phong) as tong'))
            ->get();
    }
}
