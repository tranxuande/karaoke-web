<?php

namespace App\Repositories;

use App\Models\HoaDon;
use DB;

class HoaDonRepository extends BaseRepository
{
    public function getModel()
    {
        return HoaDon::class;
    }

    public function getListCustomer($month, $year)
    {
        return $this->model
            ->select('id_khach_hang')
            ->whereMonth('ngay_tao', '=', $month)
            ->whereYear('ngay_tao', '=', $year)
            ->where('status', 1)
            ->groupby('id_khach_hang')
            ->get();
    }

    public function countBill($month, $year)
    {
        return $this->model
            ->whereMonth('ngay_tao', '=', $month)
            ->whereYear('ngay_tao', '=', $year)
            ->where('status', 1)
            ->count();
    }

    public function findHd($idR)
    {
        return $this->model
            ->where('id_phong', $idR)
            ->where('status', 0)
            ->orderByDesc('ngay_tao')
            ->first();
    }


    public function getTotalByDay($month, $year)
    {
        return
            $this->model
            ->select(DB::raw('DATE(ngay_tao) as ngay_tao, sum(tong_tien) as tong_tien'))
            ->groupBy(DB::raw('DATE(hoa_don.ngay_tao)'))
            ->whereMonth('ngay_tao', '=', $month)
            ->whereYear('ngay_tao', '=', $year)
            ->pluck('tong_tien', 'ngay_tao');
        //->get();
    }

    public function updateIdP($idR)
    {
        return $this->model->where('id_phong', $idR)->update(['id_phong' => null]);
    }
}
