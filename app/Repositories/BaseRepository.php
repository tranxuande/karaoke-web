<?php

namespace App\Repositories;

use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

abstract class BaseRepository
{
    protected $model;

    public function __construct()
    {
        $this->setModel();
    }

    abstract public function getModel();

    public function setModel()
    {
        try {
            $this->model = app()->make(
                $this->getModel()
            );
        } catch (BindingResolutionException $e) {
        }
    }

    public function getAll()
    {
        return $this->model->all();
    }

    // public function delete($attr, $value)
    // {
    //     return $this->model->where($attr, $value)->delete();
    // }

    public function create($attributes = [])
    {
        return $this->model->create($attributes);
    }

    public function find($id)
    {
        return $this->model->findOrFail($id);
    }

    public function update($data, $id)
    {
        $object = $this->model->find($id);
        return $object->update($data);
    }

    public function findByAttr($attr = [])
    {
        $result = $this->model;

        foreach ($attr as $key => $val) {
            $result = $result->where($key, $val);
        }
        return $result->first();
    }

    public function delete($attributes)
    {
        return $this->model
            ->where($attributes)
            ->delete();
    }
}
