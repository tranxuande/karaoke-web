<?php

namespace App\Repositories;

use App\Models\KhachHang;
use DB;
class KhachhangRepository extends BaseRepository
{
    public function getModel()
    {
        return KhachHang::class;
    }

    public function findBySdt($sdt)
    {
        return $this->model->where('sdt', $sdt)->first();
    }

    public function getInfo()
    {
        return $this->model
            ->leftjoin('hoa_don', 'khach_hang.id', '=', 'hoa_don.id_khach_hang')
            ->select('khach_hang.id','khach_hang.sdt', 'ten_khach_hang', DB::raw('COUNT(hoa_don.id_khach_hang) as tong'))
            ->groupBy('khach_hang.id')
            ->get();
    }
}
