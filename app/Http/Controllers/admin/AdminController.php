<?php

namespace App\Http\Controllers\admin;

use Carbon\Carbon;
use App\Helper\FormatNumber;
use App\Http\Controllers\Controller;
use App\Repositories\HoaDonRepository;
use App\Repositories\HoaDonSPRepository;
use App\Repositories\KhachhangRepository;
use App\Repositories\LoaiPhongKaraokeRepository;
use App\Repositories\PhongKaraokeRepository;
use App\Repositories\SanphamRepository;
use App\Repositories\NhapSpRepository;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use PDF;
use App\Events\PusherRoom;

class AdminController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function __construct(
        public HoaDonSPRepository  $hoaDonSPRepository,
        public FormatNumber $formatNumber,
        public HoaDonRepository $hoaDonRepository,
        public LoaiPhongKaraokeRepository $loaiPhongKaraokeRepository,
        public PhongKaraokeRepository $phongKaraokeRepository,
        public SanphamRepository $sanphamRepository,
        public NhapSpRepository $nhapSpRepository,
        public KhachhangRepository $khachhangRepository,
    ) {
    }

    public function login()
    {
        return view('admin.login');
    }

    public function postLogin(Request $request)
    {
        $credentials = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);

        if (Auth::attempt($credentials)) {
            return redirect()->route('admin.index');
        } else {
            return back()->withErrors([
                'error_login' => 'Tài khoản không đúng',
            ]);
        }
    }

    public function index(Request $request)
    {
        $month = Carbon::now()->month;
        $year = Carbon::now()->year;

        $totalByDay = $this->hoaDonRepository->getTotalByDay($month, $year);

        $totalCustomer = count($this->hoaDonRepository->getListCustomer($month, $year));
        $countBill = $this->hoaDonRepository->countBill($month, $year);
        $numberRoom = $this->phongKaraokeRepository->numberRoom($month);

        $total = 0;
        foreach ($totalByDay as $key => $value)
            $total += ($value);
        $totalRevenue = $this->formatNumber::format($total);

        $dates = $totalByDay->keys();

        $totalsByDate = $totalByDay->values();

        return view('admin.page.index', compact('totalRevenue', 'totalCustomer', 'countBill', 'month', 'dates', 'totalsByDate', 'numberRoom'));
    }

    public function filterResult(Request $request)
    {
        $month = $request->month;
        $year = $request->year;
       
        // $totalByDay = $this->hoaDonRepository->getTotalByDay($month, $year);
        // $numberRoom = $this->phongKaraokeRepository->numberRoom($month);
        // $totalCustomer = count($this->hoaDonRepository->getListCustomer($month, $year));
        // $countBill = $this->hoaDonRepository->countBill($month, $year);

        // $total = 0;
        // foreach ($totalByDay as $key => $value)
        //     $total += ($value);
        // $totalRevenue = $this->formatNumber::format($total);

        // $dates = $totalByDay->keys();

        // $totalsByDate = $totalByDay->values();

        $totalByDay = $this->hoaDonRepository->getTotalByDay($month, $year);

        $totalCustomer = count($this->hoaDonRepository->getListCustomer($month, $year));
        $countBill = $this->hoaDonRepository->countBill($month, $year);
        $numberRoom = $this->phongKaraokeRepository->numberRoom($month);

        $total = 0;
        foreach ($totalByDay as $key => $value)
            $total += ($value);
        $totalRevenue = $this->formatNumber::format($total);

        $dates = $totalByDay->keys();

        $totalsByDate = $totalByDay->values();

        return response()->json([
            'totalRevenue' => $totalRevenue,
            'totalCustomer' => $totalCustomer,
            'totalBill' => $countBill,
            'dates' => $dates,
            'totalsByDate' => $totalsByDate,
            'numberRoom' => $numberRoom,
        ]);
    }

    // room
    public function createRoom()
    {
        $listKindRoom = $this->loaiPhongKaraokeRepository->getAll();

        return view('admin.page.room.create', compact('listKindRoom'));
    }

    public function postCreateRoom(Request $request)
    {
        $file = $request->file('file');
        $uploadFile = Storage::disk('public')->putFileAs('image', $file, $file->getClientOriginalName());
        $path = Storage::url($uploadFile);

        $data = [
            'image' => $path,
            'ten_phong' => $request->ten_phong,
            'id_loai_phong' => $request->loai_phong,
            'gia_1_gio' => $request->gia,
            'so_luong_nguoi' => $request->so_luong_nguoi,
        ];

        $createRoom = $this->phongKaraokeRepository->create($data);

        if ($createRoom) {
            return redirect()->route('admin.list-room')->with('mess', 'Tạo phòng thành công');
        }
    }

    public function listRoom()
    {
        $listRoom = $this->phongKaraokeRepository->getListRoom();

        return view('admin.page.room.list', compact('listRoom'));
    }

    public function editRoom($id)
    {
        $room = $this->phongKaraokeRepository->find($id);

        $listKindRoom = $this->loaiPhongKaraokeRepository->getAll();

        return view('admin.page.room.edit', compact('room', 'listKindRoom'));
    }

    public function postEditRoom(Request $request, $id)
    {
        $file = $request->file('file');
        $data = [
            'ten_phong' => $request->ten_phong,
            'id_loai_phong' => $request->loai_phong,
            'gia_1_gio' => $request->gia
        ];

        if ($file) {
            $uploadFile = Storage::disk('public')->putFileAs('image', $file, $file->getClientOriginalName());
            $path = Storage::url($uploadFile);
            $data['image'] = $path;
        }

        $updateRoom = $this->phongKaraokeRepository->update($data, $id);

        if ($updateRoom) {
            return redirect()->route('admin.list-room')->with('mess', 'Sửa phòng thành công');
        }
    }

    public function emptyRoom()
    {
        $loaiPhong = $this->loaiPhongKaraokeRepository->getAll();
        $phong = $this->phongKaraokeRepository->getAll();
        return view('admin.page.room.empty', compact('loaiPhong', 'phong'));
    }

    public function cancel($idhd)
    {
        $listSpInBill = $this->hoaDonSPRepository->getByIDHD($idhd);

        foreach ($listSpInBill as $i) {
            $sp = $this->sanphamRepository->find($i->id_sp);
            $updateSp = $this->sanphamRepository->update([
                'so_luong_co' => $sp->so_luong_co + $i->so_luong_ban,
                'tinh_trang' => 0
            ], $i->id_sp);
        }
        $hd = $this->hoaDonRepository->find($idhd);
        $phong = $this->phongKaraokeRepository->update(['tinh_trang' => 0], $hd->id_phong);
        event(new PusherRoom(['idR' => $hd->id_phong]));

        $deleteSpInBill = $this->hoaDonSPRepository->delete('id_hoadon', $idhd);
        $deletehd = $this->hoaDonRepository->delete('id', $idhd);

        return response()->json(['stauts' => 200]);
    }

    public function deleteRoom($id)
    {
        $findRoom = $this->phongKaraokeRepository->find($id);

        if ($findRoom->tinh_trang == 0) {
            $updateHd = $this->hoaDonRepository->updateIdP($id);
            $deleteP = $this->phongKaraokeRepository->delete(['id' => $id]);

            return response()->json(['status' => 200]);
        } else
            return response()->json(['status' => 404]);
    }
    public function findHd($idR)
    {
        $hd = $this->hoaDonRepository->findHd($idR);

        return response()->json(['hd' => $hd]);
    }

    public function startRoom($id)
    {
        $hd = $this->hoaDonRepository->find($id);

        $updateHd = $this->hoaDonRepository->update(
            [
                'thoi_gian_vao' => Carbon::now(),
            ],
            $id
        );

        $updatePhong = $this->phongKaraokeRepository->update(['tinh_trang' => 1], $hd->id_phong);

        return response()->json(['hd' => $this->hoaDonRepository->find($id)]);
    }

    // product
    public function listProduct()
    {
        $list = $this->sanphamRepository->getAll();
        return view('admin.page.product.list', compact('list'));
    }

    public function addProduct(Request $request)
    {

        $data = [
            'ten_san_pham' => $request->ten_sp,
            'loai_san_pham' => $request->loai_sp
        ];

        $this->sanphamRepository->create($data);

        return redirect()->back();
    }

    public function editProduct(Request $request)
    {
        $data = [
            'ten_san_pham' => $request->ten_sp,
            'loai_san_pham' => $request->loai_sp,
        ];

        $update = $this->sanphamRepository->update($data, $request->id);

        if ($update) {
            return redirect()->back();
        }
    }

    public function listImportProduct()
    {
        $listProduct = $this->nhapSpRepository->getAll();

        return view('admin.page.product.list-import', compact('listProduct'));
    }

    public function importProduct()
    {
        $list = $this->sanphamRepository->getAll();
        return view('admin.page.product.import', compact('list'));
    }

    public function postimportProduct(Request $request)
    {
        $data = [
            'id_sp' => $request->id_sp,
            'so_luong_nhap' => $request->so_luong_nhap,
            'so_luong_1_sp' => $request->so_luong_1_sp,
            'gia_nhap' => $request->gia_nhap,
            'don_vi_nhap' => $request->don_vi_nhap,
            'thoi_gian_nhap' => Carbon::now()
        ];

        $importProduct = $this->nhapSpRepository->create($data);

        $findSp = $this->sanphamRepository->find($request->id_sp);
        $dataUPdate = [
            'so_luong_co' => (!is_null($request->so_luong_1_sp) ? $request->so_luong_nhap * $request->so_luong_1_sp : $request->so_luong_nhap) + $findSp->so_luong_co,
            'tinh_trang' => 1
        ];

        $updateSp = $this->sanphamRepository->update($dataUPdate, $request->id_sp);

        if ($importProduct && $updateSp) {
            return redirect()->route('admin.list-import-product')->with('mess', 'sds');
        }
    }

    public function listExportProduct()
    {
        $listProduct = $this->sanphamRepository->getExSP();

        return view('admin.page.product.list-export', compact('listProduct'));
    }

    public function exportProduct($id)
    {
        $product = $this->sanphamRepository->find($id);

        return view('admin.page.product.export', compact('product'));
    }

    public function postExportProduct(Request $request, $id)
    {
        $product = $this->sanphamRepository->find($id);

        $data = [
            'gia_ban' => $request->gia_ban,
            'don_vi_ban' => $request->don_vi_ban,
        ];

        if ($product->loai_san_pham == 0) {
            $data['dinh_luong'] = $request->dinh_luong;
            if ($request->don_vi_dinh_luong == 'g') {
                $data['dinh_luong'] = $request->dinh_luong / 1000;
            }
        }

        $updateProduct = $this->sanphamRepository->update($data, $id);

        if ($updateProduct) {
            return redirect()->route('admin.list-export-product')->with('mess', 'sds');
        }
    }

    public function detailHd($idHd)
    {
        $hd = $this->hoaDonRepository->find($idHd);
        $phong = $this->phongKaraokeRepository->find($hd->id_phong);
        $listSpBill = $this->sanphamRepository->listSpBill($idHd);

        $thoiGianSuDung = strtotime(Carbon::now()) - strtotime($hd->thoi_gian_vao);

        $tongTienPhong = round(($thoiGianSuDung / 3600) * $phong->gia_1_gio, 0);

        $tongDv = 0;
        foreach ($listSpBill as $item) {
            $tongDv += $item->gia_ban * $item->so_luong_ban;
        }

        $tong = $tongDv + $tongTienPhong;

        $data = [
            'thoi_gian_ra' => Carbon::now(),
            'status' => 1,
            'tong_tien' => $tong
        ];

        $this->hoaDonRepository->update($data, $idHd);
        $this->phongKaraokeRepository->update(['tinh_trang' => 0], $phong->id);
        $hd = $this->hoaDonRepository->find($idHd);

        event(new PusherRoom(['idR' => $phong->id]));

        $pdf = PDF::loadView('user.pdf', compact('listSpBill', 'hd', 'phong', 'tongTienPhong', 'tongDv', 'tong'));
        $encoded_filename = rawurlencode($phong->ten_phong);

        return response($pdf->output(), 200)
            ->header('Content-Type', 'application/pdf')
            ->header('Content-Disposition', 'attachment; filename="' . $encoded_filename . '.pdf"');
        // return $pdf->download('sv.pdf');
        // return $pdf->stream();
        // return view('admin.page.bill.detail', compact('listSpBill', 'hd', 'phong', 'tongTienPhong'));
    }

    public function customer()
    {
        $listUser = $this->khachhangRepository->getInfo();
        return view('admin.page.customer', compact('listUser'));
    }
}
