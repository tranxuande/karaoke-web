<?php

namespace App\Http\Controllers;

use App\Events\PusherRoom;
use App\Events\RoomRented;
use App\Http\Controllers\Controller;
use App\Repositories\HoaDonRepository;
use App\Repositories\HoaDonSPRepository;
use App\Repositories\KhachhangRepository;
use App\Repositories\LoaiPhongKaraokeRepository;
use App\Repositories\PhongKaraokeRepository;
use App\Repositories\SanphamRepository;
use Carbon\Carbon;
use GuzzleHttp\Promise\Create;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
class UserController extends Controller
{
    public function __construct(
        public HoaDonRepository $hoaDonRepository,
        public PhongKaraokeRepository $phongKaraokeRepository,
        public KhachhangRepository $khachhangRepository,
        public LoaiPhongKaraokeRepository $loaiPhongKaraokeRepository,
        public SanphamRepository $sanphamRepository,
        public HoaDonSPRepository $hoaDonSPRepository,
    ) {
    }

    public function login()
    {
        return view('user.login');
    }

    public function postLogin(Request $request)
    {

        $sdt = $request->sdt;

        $checkLogin = $this->khachhangRepository->findBySdt($sdt);
        session()->put('account', $checkLogin);
        if ($checkLogin) {
            return redirect()->route('home');
        }
        return redirect()->route('register');
    }

    public function register()
    {
        return view('user.register');
    }

    public function postRegister(Request $request)
    {
        $data = [
            'email' => $request->email,
            'sdt' => $request->sdt,
            'gioi_tinh' => $request->gt,
            'ten_khach_hang' => $request->name,
        ];

        $create = $this->khachhangRepository->create($data);
        session()->put('account', $create);

        if ($create) {
            return redirect()->route('home');
        }
        return 0;
    }

    public function home()
    {
        $loaiPhong = $this->loaiPhongKaraokeRepository->getAll();
        $phong = $this->phongKaraokeRepository->getAll();

        return view('user.home', compact('loaiPhong', 'phong'));
    }

    public function detail($id)
    {
        // $food = $this->sanphamRepository->findFood(0);
        $food = $this->sanphamRepository->getExSP(0);

        // $drink = $this->sanphamRepository->findFood(1);
        $drink = $this->sanphamRepository->getExSP(1);

        $room = $this->phongKaraokeRepository->find($id);
        return view('user.detail', compact('food', 'drink', 'room'));
    }

    public function confirm($id, Request $request)
    {
        session()->put('idr', $id);
        if (isset($request->ids) && isset($request->sl) && isset($request->food) && isset($request->money)) {
            session()->put('ids', $request->ids);
            session()->put('sls', $request->sl);
            session()->put('foods', $request->food);
            session()->put('moneys', $request->money);
        }
        return view('user.confirm');
    }

    public function postConfirm(Request $request)
    {
        $findkH = $this->khachhangRepository->findByAttr(['sdt' => $request->sdt]);

        if (!$findkH) {
            $createCustom = $this->khachhangRepository->create([
                'ten_khach_hang' => $request->tkh,
                'sdt' => $request->sdt,
                'email' => $request->email
            ]);
        } else {
            $updateCustom = $this->khachhangRepository->update([
                'ten_khach_hang' => $request->tkh,
                'email' => $request->email,
            ], $findkH->id);
        }

        $dataHd = [
            // 'thoi_gian_vao' => Carbon::now(),
            'thoi_gian_dat' => $request->timeInput,
            'ngay_tao' => Carbon::now(),
            'id_khach_hang' => !$findkH ? $createCustom->id : $findkH->id,
            'id_phong' => session('idr'),
        ];

        $createHd = $this->hoaDonRepository->create($dataHd);
        session()->put('hd', $createHd);

        if (session()->has('ids')) {
            foreach (session('ids') as $key => $val) {
                $data = [
                    'id_hoadon' => $createHd->id,
                    'id_sp' => session('ids')[$key],
                    'so_luong_ban' => session('sls')[$key]
                ];
                $tongTien[$key] = session('moneys')[$key] * session('sls')[$key];
                $createHdSp = $this->hoaDonSPRepository->create($data);

                $spUp = $this->sanphamRepository->find(session('ids')[$key]);
                $this->sanphamRepository->update(
                    [
                        'so_luong_co' => $spUp->so_luong_co - (is_null($spUp->dinh_luong) ? session('sls')[$key] : session('sls')[$key] * $spUp->dinh_luong),
                    ],
                    session('ids')[$key]
                );
            }

            session()->put('tongTien', $tongTien);
        }


        $updateRoom = $this->phongKaraokeRepository->update(['tinh_trang' => 2], session('idr'));

        event(new PusherRoom(['idR' => session('idr'), 'idHd' => $createHd->id, 'statusRoom' => 2]));
        $hd = $this->hoaDonRepository->find($createHd->id);
        event(new RoomRented($createHd->id_phong, $request->timeInput));

        // $data = array('name'=> $request->name, 'message' => 'Cảm ơn bạn đã đặt phòng!');
        Mail::send('user.email', ['name'=> $request->tkh] , function($message) use ($request) {
            $message->to($request->email, $request->name)
                    ->subject('Xác nhận đặt phòng');
        });

        return view('user.confirm')->with('mess','ok');
    }

    public function bill()
    {
        $phong = $this->phongKaraokeRepository->find(session('hd')['id_phong']);

        return view('user.bill', compact('phong'));
    }

    public function getBill($idHd)
    {
        $hdBill = $this->phongKaraokeRepository->hdBill($idHd);

        $listSpBill = $this->sanphamRepository->listSpBill($idHd);

        return response()->json(['hdBill' => $hdBill, 'listSpBill' => $listSpBill]);
    }
}

// mxnsmrcbxcwspngb
