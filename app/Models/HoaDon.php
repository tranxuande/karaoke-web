<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HoaDon extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $table = 'hoa_don';

    protected $fillable =[
        'thoi_gian_vao',
        'thoi_gian_ra',
        'ngay_tao',
        'id_khach_hang',
        'id_phong',
        'status',
        'tong_tien',
        'thoi_gian_dat',
    ];


}
