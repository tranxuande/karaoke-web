<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KhachHang extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $table = 'khach_hang';
    
    protected $fillable =[
        'email',
        'gioi_tinh',
        'sdt',
        'ten_khach_hang',
    ];
}
