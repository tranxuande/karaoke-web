<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LoaiPhongKaraoke extends Model
{
    use HasFactory;
    public $timestamps = false;

    protected $table = 'loai_phong_karaoke';
}
