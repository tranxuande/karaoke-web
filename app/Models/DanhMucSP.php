<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DanhMucSP extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $table = 'danh_muc_san_pham';
   
}
