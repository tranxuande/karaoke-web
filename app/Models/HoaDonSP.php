<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HoaDonSP extends Model
{
    use HasFactory;
    protected $table = 'hoadon_sanpham';
    protected $fillable =[
        'id_hoadon',
        'id_sp',
        'so_luong_ban',
        
    ];
    public $timestamps = false;
}
