<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\SanPham;
class NhapSp extends Model
{
    use HasFactory;
    public $timestamps = false;

    protected $table = 'nhap_hang';

    protected $fillable =[
        'id_sp',
        'so_luong_nhap',
        'don_vi_nhap',
        'gia_nhap',
        'so_luong_1_sp',
        'thoi_gian_nhap',
        'dinh_luong',
    ];

    public function sanPham()
    {
        return $this->belongsTo(SanPham::class,'id_sp','id');
    }
}
