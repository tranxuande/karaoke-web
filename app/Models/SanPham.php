<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\NhapSp;
class SanPham extends Model
{
    use HasFactory;
    public $timestamps = false;
    
    protected $table = 'san_pham';

    protected $fillable =[
        'ten_san_pham',
        'so_luong_nhap',
        'don_vi_nhap',
        'gia_nhap',
        'gia_ban',
        'don_vi_ban',
        'tinh_trang',
        'so_luong_1_sp',
        'loai_san_pham',
        'thoi_gian_nhap',
        'dinh_luong',
        'so_luong_co',
    ];

    public function nhapHangs()
    {
        return $this->hasMany(NhapSp::class);
    }
}
