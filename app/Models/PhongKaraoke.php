<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PhongKaraoke extends Model
{
    use HasFactory;
    public $timestamps = false;

    protected $table = 'phong_karaoke';

    protected $fillable =[
        'image',
        'ten_phong',
        'gia_1_gio',
        'id_loai_phong',
        'tinh_trang',
        'so_luong_nguoi',
    ];
}
