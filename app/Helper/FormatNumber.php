<?php

namespace App\Helper;

class FormatNumber
{
    public static function format($size, $precision = 3)
    {
        // $base = 1000;
        // $units = array('B','K', 'M', 'G', 'T');
        // $unit = 0;

        // while ($size >= $base && $unit < count($units) - 1) {
        //     $size /= $base;
        //     $unit++;
        // }
        // $size = round($size, $precision);
        // if ($unit == 2) {
        //     $size *= 1000; // convert to bytes
        // } elseif ($unit == 3) {
        //     $size *= 1000000; // convert to bytes
        // }
        // return number_format($size, $precision) . ' ' . $units[$unit];

        if ($size < 1000) {
            return number_format($size, $precision) ;
        } elseif ($size < 1000000) {
            return number_format($size / 1000, $precision) ;
        } elseif($size<1000000000) {
            return number_format($size / 1000000, $precision) . ' M';
        }else {
            return number_format($size / 1000000000, $precision) . ' G';
        }
    }
}
